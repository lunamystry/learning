# 10 000 hours

According to... I wanna say [Malcom Gladwell], deliberate practice is important
in the mastery of a skill. This repo is where I do my practice of the Software
Skill.

## Structure

I've divided the areas of practice into three:

- tools - this is languages, frameworks and other tools that I may be learning
- tutorials - going through tutorials
- problems - this is where I attempt problems from sites such as exercism, hackerank, codility and leetcode
- algorithms - this is notes and examples on algorithms that I may be learning

## Talks

I gave talk for the Dariel Speaker Forge on the 29th July 2021. You can find the PDF here [what I am loving about rust]

I gave talk for the Dariel Speaker Forge on the 27th July 2023. You can find the PDF here [Optional: Is it a container?]

[malcom gladwell]: https://nationalpost.com/news/canada/malcolm-gladwell-got-it-wrong-deliberate-practice-not-10000-hours-key-to-achievement-psychologist-says
[what I am loving about rust]: presentations/what_am_loving_about_rust/what_am_loving_about_rust.pdf
[Optional: Is it a container?]: presentations/modern_programming_tools/decks/optional/Optional_Is_it_a_container.pdf
