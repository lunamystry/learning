# Algorithms

This directory contains algorithms I have learned and a list of those I am yet
to learn are listed below. The list comes from [interviews.school]

# TODO

- Time complexity
  Big O notation. How does the time taken to execute change depending on size of input

- Space complexity
  Big O notation. How does the space needed change depending on the size on input

* General sorting
* Quicksort, mergesort
* Sets and Maps
  There are 2 kinds of sets/maps? does Python/Haskell have both?

  - hash table based (has O(1) basic operations)
  - tree based (has interesting functions like higherEntry in Java)

* Stack
  The push and pop thing (why is it represented by plates)

  - Need to make sure I can write my own with operation in O(1)
  - Last In First Out

* Queue

  - Last In Last Out

* Priority Queue

  - Can arbitrary comparator be used?

* Linked List

  - a list made up of pointers to try avoid the space problem with arrays

- Recursion and backtracking
- Bit operations
- Graphs
- Depth-first Search
- Breath-first Search
- Trees
- Greedy
- Trie
- Dynamic Programming
- Union Find
- Binary Search
- Math
- Strings
- Two pointers

[inteviews.schools]: https://interviews.school/algos
