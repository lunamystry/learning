module Main where

import           Data.Map                       ( (!)
                                                , Map
                                                )
import qualified Data.Map                      as Map


data Graph a = G [a] (BRfun a)
type BRfun a = a -> [a]

dfsearch source (G _ sucs) = reverse (search [] source)
 where
  search visited v =
    if v `elem` visited then visited else foldl search (v : visited) (sucs v)

main :: IO ()
main = undefined
