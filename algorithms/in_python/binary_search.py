import math


def is_in(needle, haystack):
    """A function that does binary search"""
    if not haystack:
        return False
    if len(haystack) == 1 and haystack[0] != needle:
        return False

    sorted_haystack = sorted(haystack)
    haystack_size = len(sorted_haystack)
    halfway_index = math.floor(haystack_size / 2)
    halfway_item = sorted_haystack[halfway_index]

    if needle == halfway_item:
        return True
    elif needle > halfway_item:
        return is_in(needle, haystack[:halfway_index])
    elif needle < halfway_item:
        return is_in(needle, haystack[halfway_index:])
    else:
        return False


if __name__ == "__main__":
    print(is_in(5, [8]))
    print(is_in(5, [5]))
    print(is_in(5, [8, 5]))
    print(is_in(5, [5, 5]))
    print(is_in(5, [8, 2, 9, 0, 5]))
    print(is_in(5, []))
