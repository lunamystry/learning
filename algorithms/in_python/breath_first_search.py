import collections
from queue import Queue

def bfs(graph, start):
    """breath first search"""
    q = Queue()
    seen = set()
    q.put(start)

    while not q.empty():
        nxt = q.get()
        print(f"processing {nxt}")
        for node in graph[nxt]:
            if node not in seen and node not in q.queue:
                q.put(node)
        seen.add(nxt)

if __name__ == "__main__":
    graph = {
        "A": ["B", "C", "D"],
        "B": ["A", "D"],
        "C": ["A", "D"],
        "D": ["A", "B", "C", "E"],
        "E": ["D"],
    }

    graph2 = {
        "D": ["H", "E"],
        "H": ["D", "E", "I"],
        "I": ["H", "F"],
        "E": ["D", "H", "A"],
        "A": ["E", "B"],
        "B": ["A", "F"],
        "F": ["B", "I", "G", "J"],
        "J": ["F", "G"],
        "G": ["F", "J", "C"],
        "C": ["G"],
    }

    graph3 = {
        0: [1, 2, 3],
        1: [0, 2],
        2: [0, 1, 4],
        3: [0],
        4: [2],
    }

    # better tests are needed hhawa
    bfs(graph, "D")
    bfs(graph2, "D")
    bfs(graph3, 0)
