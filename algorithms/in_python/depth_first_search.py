def dfs(graph, start, visited=[]):
    """Depth first search"""

    # this works because visited points to the same object in every call
    visited.append(start)
    for node in graph[start]:
        if node not in visited:
            dfs(graph, node)

    return visited


def dfs_iter(graph, start, visited=[]):
    """Depth first search iteratively with a stack"""
    stack = [start]
    while len(stack) > 0:
        node = stack.pop()
        if node not in visited:
            visited.append(node)
            for neighbour in graph[node]:
                if neighbour not in visited:
                    stack.append(neighbour)
    return visited


if __name__ == "__main__":
    # undirected graph
    graph = {
        "A": ["B", "C", "D"],
        "B": ["A", "D"],
        "C": ["A", "D"],
        "D": ["A", "B", "C", "E"],
        "E": ["D"],
    }
    print(dfs(graph, "D"))
    print(dfs_iter(graph, "D"))
