import math


def merge(A, B):
    """Merge two sorted lists"""
    i_a = 0
    i_b = 0
    result = []

    while i_a < len(A) and i_b < len(B):
        if A[i_a] <= B[i_b]:
            result.append(A[i_a])
            i_a += 1
        elif B[i_b] < A[i_a]:
            result.append(B[i_b])
            i_b += 1

    if i_a < len(A):
        result += A[i_a:]

    if i_b < len(B):
        result += B[i_b:]

    return result


def sort(A):
    """perform the merge sort"""
    if len(A) <= 1:
        return A

    mid_index = math.floor(len(A) / 2)

    left = sort(A[mid_index:])
    right = sort(A[:mid_index])
    return merge(left, right)


if __name__ == "__main__":
    print(sort([3, 1, 9, 102, 5, -3, -98, 50]))
    print(sort([1, 1, 1, 1]))
