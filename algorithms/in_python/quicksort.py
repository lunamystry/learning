def pivot(array, start, end):
    """ the function that can be recursed to do the quicksort"""
    print(f"pivoting: {array} from {start} to {end}")
    pivot = array[end]
    swap_point = start

    for i in range(start, end):
        if array[i] < pivot:
            if i != swap_point:
                print(f" swap: {array[i]}@{i} and {array[swap_point]}@{swap_point} ")
                array[i], array[swap_point] = array[swap_point], array[i]
            swap_point += 1

    print(f"swap: {array[end]}@{end} and {array[swap_point]}@{swap_point}")
    array[end], array[swap_point] = array[swap_point], array[end]
    return (swap_point, array)


def sort(array, start, end):
    if start >= end:
        return array

    p, _ = pivot(array, start, end)
    sort(array, start, p - 1)  # left side
    sort(array, p + 1, end)  # right side

    return array  # array sorted in place


def quicksort(array):
    """function to quickly sort in an average of logn time"""
    return sort(array, 0, len(array) - 1)


if __name__ == "__main__":
    arr = [-100, 909, 3, 728, 34, 0, -8]
    print(f"\nhow it started: {arr}\n")
    quicksort(arr)
    print(f"\nhow it's going: {arr}\n")
