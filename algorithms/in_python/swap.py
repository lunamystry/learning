def counting(A, m):
    n = len(A)
    count = [0] * (m + 1)
    for k in range(n):
        count[A[k]] += 1

    return count


def fast(A, B, m):
    n = len(A)
    sum_a = sum(A)
    sum_b = sum(B)
    d = sum_b - sum_a
    if d % 2 == 1:
        print(f"difference {d} is not multiple of 2 [{sum_a}, {sum_b}]")
        return False

    d //= 2
    count = counting(A, m)
    for i in range(n):
        if 0 <= B[i] - d and B[i] - d <= m and count[B[i] - d] > 0:
            return True
    return False


if __name__ == "__main__":
    print(fast([2, 3, 1], [4, 3, 1], 3))
