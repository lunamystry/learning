package me.mandla
package algorithms

package courseschedule

object Problem:
  val description =
    """
  You are given an array prerequisites where prerequisites[i] = [a, b] indicates
  that you must take course b first if you want to take course a.

  The pair [0, 1], indicates that must take course 1 before taking course 0.

  There are a total of numCourses courses you are required to take, labeled
  from 0 to numCourses - 1.

  Return true if it is possible to finish all courses, otherwise return false.

  Example:
    Input: numCourses = 2, prerequisites = [[0,1]]
    Output: true
    Explanation: First take course 1 (no prerequisites) and then take course 0.

  Example 2:
    Input: numCourses = 2, prerequisites = [[0,1],[1,0]]
    Output: false
    Explanation: In order to take course 1 you must take course 0, and to take
    course 0 you must take course 1. So it is impossible.

  """
  def examples() =
    println("=" * 60)
    example1()
    println("=" * 60)
    example2()

  def example1() =
    val numOfCourses: Int = 2
    val prerequisites: List[(Int, Int)] = List((0, 1))
    println(s"Input: numCourses = $numOfCourses $prerequisites")
    val output: Boolean = solution(numOfCourses, prerequisites)
    println(s"Output: $output")

  def example2() =
    val numOfCourses: Int = 2
    val prerequisites: List[(Int, Int)] = List((0, 1), (1, 0))
    println(s"Input: numCourses = $numOfCourses $prerequisites")
    val output: Boolean = solution(numOfCourses, prerequisites)
    println(s"Output: $output")

  def solution(numOfCourses: Int, prerequisites: List[(Int, Int)]): Boolean =
    // create a prerequisites map from the list
    var preMap =
      prerequisites.foldLeft(Map.empty[Int, List[Int]]) { (a, c) =>
        a.get(c._1)
          .fold(a.updated(c._1, List(c._2))) { (list: List[Int]) =>
            a.updated(c._1, list :+ c._2)
          }
      }

    def canBeCompleted(
      course: Int,
      visited: Set[Int],
    ): Boolean =
      println
      println("-" * 25)
      println(s"course: $course")
      println(s"visited: $visited")
      println(s"prerequisites map: $preMap")
      // if visited, that means it is a cycle and can't be completed
      if visited.contains(course) then return false
      // if it does not have prerequisites, it can be completed
      if preMap.get(course).isEmpty then return true

      // for each prerequisite check if it can be completed
      for prerequisite <- preMap.getOrElse(course, List()) do
        if !canBeCompleted(prerequisite, visited + course)
        then
          println(s"course $course cannot be completed")
          return false

      println(s"course $course can be completed")
      preMap = preMap.updated(course, List())
      true

    preMap.keySet.forall(course => canBeCompleted(course, Set.empty[Int]))
