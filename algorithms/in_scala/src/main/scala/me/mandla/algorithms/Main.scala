package me.mandla
package algorithms

import courseschedule.Problem

@main def Main(args: String*): Unit =
  println("─" * 100)

  println(Problem.description)
  Problem.examples()

  println("─" * 100)
