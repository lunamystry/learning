package me.mandla
package algorithms

import courseschedule.Problem

final class CourseScheduleSuite extends TestSuite:
  type NumOfCourses =
    Int
  type Prerequisites =
    List[(Int, Int)]
  type Output =
    Boolean
  type Input =
    (NumOfCourses, Prerequisites, Output)

  for (numOfCourses, prerequisites, expectedOutput) <-
        List[Input](
          (2, List((0, 1)), true),
          (4, List((0, 1), (2, 1), (2, 3), (2, 0)), true),
          (2, List((0, 1), (1, 0)), false),
          (5, List((2, 3), (2, 4), (4, 5), (5, 1), (1, 2)), false),
        )
  do
    test(s"${if expectedOutput then "can" else "cannot"} be completed $prerequisites"):
      println(s"Input: numCourses = $numOfCourses $prerequisites")
      val output: Boolean = Problem.solution(numOfCourses, prerequisites)
      println(s"Output: $output")

      assert(output == expectedOutput)
