package me.mandla
package tree

sealed trait Tree[+A]
case class Node[A](value: A, children: List[Tree[A]]) extends Tree[A]
case class Leaf[A](value: A) extends Tree[A]

extension [A](t: Tree[A])
  def prune[B](a: Tree[B]): Tree[B] =
    a match
      case Leaf(v) => Leaf(v)
      case Node(v, children) =>
        if children.isEmpty
        then Leaf(v)
        else Node(v, children.map(prune))

  def map[B](mapper: A => B): Tree[B] =
    val m = t match
      case Leaf(a) => Leaf(mapper(a))
      case Node(a, children) =>
        // this is a pre-order traversal
        val ma = mapper(a)
        Node(ma, children.map(c => c.map(mapper)))
    prune(m)

  def flatMap[B](mapper: A => Tree[B]): Tree[B] =
    val m = t match
      case Leaf(a) => mapper(a)
      case Node(a, children) =>
        // this is a pre-order traversal
        val b: Tree[B] = mapper(a)
        children.map(c => c.flatMap(mapper)) match
          case Nil => b
          case leaves: List[Tree[B]] =>
            b match
              case Leaf(v) => Node(v, leaves)
              case Node(v, ls) => Node(v, ls ++ leaves)
    prune(m)
