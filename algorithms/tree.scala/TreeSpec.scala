package me.mandla
package tree

import me.mandla.tree.*

import org.scalatest.*
import flatspec.*
import matchers.*

class TreeSpec extends AnyFlatSpec with should.Matchers:
  "Tree" should "be mappable" in {
    val tree: Tree[Int] = Node(
      1,
      List(
        Node(
          2,
          List(
            Leaf(4),
            Leaf(5),
          ),
        ),
        Node(
          3,
          List(
            Leaf(6),
            Leaf(7),
          ),
        ),
      ),
    )
    val doubled = tree.map(_ * 2)

    val split: Int => Tree[Int] =
      a =>
        if a % 2 == 0
        then Node(0, List(Leaf(a / 2), Leaf(a / 2)))
        else Node(a, List())
    val splitTree = tree.flatMap(split)

    println(tree)
    println(doubled)
    println(splitTree)
  }
