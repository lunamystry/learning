package me.mandla.sis;

interface Runnable {
  public void run();
}

class SisRunnable implements Runnable {

  private String state;

  public static void main() {
    System.out.println("main is the reason static exists?");
  }

  public void doSomething() {
    System.out.println("doing something with the state");
  }

  public void run() {
    System.out.println("running");
  }
}
