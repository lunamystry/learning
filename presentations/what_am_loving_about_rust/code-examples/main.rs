#![warn(missing_docs)]
//! This is a demonstration of some of Rust's features

/// Adds 2 to a given number n
///
/// ```
///  use playground::add_two;
///
///  let result = add_two(3);
///  assert_eq!(result, 5);
/// ```
pub fn add_two(n: i32) -> i32 {
    internal_adder(n, 3)
}

fn internal_adder(a: i32, b: i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn internal() {
        assert_eq!(4, internal_adder(2, 2));
    }
}

fn main() {
    let one = 1;

    println!("we are number {}", one);
}
