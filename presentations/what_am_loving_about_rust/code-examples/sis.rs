mod sis;

trait Runnable {
    fn run();
}

struct Sis {
    state: String;
}

impl Sis {
    fn do_something () {
        println!("doing something with the state");
    }
}

impl Runnable for Sis {
    fn run() {
        println!("running")
    }
}

impl Runnable for bool {
    fn run() {
        println!("why did the bools run? because they saw red!")
    }
}

fn main() {
    println!("mainly")
}
