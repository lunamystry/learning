def angryProfessor(k, a):
    """Just you know stop being angry dude"""
    on_time = len([i for i in a if i <= 0])
    if on_time < k:
        return "YES"
    else:
        return "NO"


def rotate(a):
    """take the thing at the end, move it to the beginning"""
    b = a[:-1]
    b.insert(0, a[-1])
    return b


def circularArrayRotation(a, k, queries):
    """rotate and tell me what's there"""
    restriced_k = k % len(a)
    print(restriced_k, "vs", k)
    for _ in range(restriced_k):
        a = rotate(a)
    return [a[i] for i in queries]


a = [1, 2, 3]
k = 302
queries = [0, 1, 2]
circularArrayRotation(a, k, queries)

print(a)
