module Main where

import           Control.Monad
import           Data.List
import           System.IO

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  heights <- replicateM 8 $ do
    input_line <- getLine
    let mountainH = read input_line :: Int
    return mountainH
  let heighest = maximum $ heights
  putStrLn . show . head . elemIndices heighest $ heights
