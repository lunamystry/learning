module Main where

data Trajectory = Mountain | Valley | Sealevel
  deriving (Eq, Show)

type Step = Int

data State = State
  { _accumulation :: Int
  , _valleyCount  :: Int
  , _trajectory   :: Trajectory
  }
  deriving Show

defaultState :: State
defaultState = State 0 0 Sealevel

readStep :: Char -> Step
readStep c | c == 'U'  = 1
           | c == 'D'  = -1
           | otherwise = 0

trajectory :: Int -> Trajectory -> Trajectory
trajectory newAcc currTrajectory
  | newAcc > 0 && currTrajectory == Sealevel = Mountain
  | newAcc < 0 && currTrajectory == Sealevel = Valley
  | newAcc == 0 = Sealevel
  | otherwise   = currTrajectory

valleyCount :: Trajectory -> Trajectory -> Int -> Int
valleyCount newTrajectory currTrajectory currentCount
  | newTrajectory == Valley && currTrajectory == Sealevel = currentCount + 1
  | otherwise = currentCount

updateState :: State -> Step -> State
updateState s st = State
  { _accumulation = newAccumulation
  , _trajectory   = newTrajectory
  , _valleyCount  = valleyCount newTrajectory (_trajectory s) (_valleyCount s)
  }
 where
  newAccumulation = _accumulation s + st
  newTrajectory   = trajectory newAccumulation (_trajectory s)

main :: IO ()
main =
  interact
    $ show
    . _valleyCount
    . foldl updateState defaultState
    . map readStep
    . last
    . lines


-- Less overkill solution from Hackerrank

-- -- Enter your code here. Read input from STDIN. Print output to STDOUT
-- countValleys [] _ = 0
-- countValleys (x:xs) level
--     | x == 'U' && level == -1 = 1 + countValleys xs (level + 1)
--     | x == 'D' = countValleys xs (level - 1)
--     | x == 'U' = countValleys xs (level + 1)


-- main = do
--     q <- getLine
--     steps <- getLine
--     print $ countValleys steps 0
