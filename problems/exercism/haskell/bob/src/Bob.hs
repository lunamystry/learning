{-# LANGUAGE OverloadedStrings #-}

module Bob
  ( responseFor
  ) where

import           Data.Char                      ( isAlpha
                                                , isUpper
                                                )
import           Data.Text                      ( Text
                                                , all
                                                , filter
                                                , isSuffixOf
                                                , null
                                                , strip
                                                )
import           Prelude                 hiding ( all
                                                , filter
                                                , null
                                                )

isYelling :: Text -> Bool
isYelling = yelling . filter isAlpha
  where yelling xs = all isUpper xs && not (null xs)

isQuestion :: Text -> Bool
isQuestion = isSuffixOf "?" . strip

isNotSayingAnything :: Text -> Bool
isNotSayingAnything = null . strip


responseFor :: Text -> Text
responseFor xs | questioning && yelling = "Calm down, I know what I'm doing!"
               | questioning            = "Sure."
               | yelling                = "Whoa, chill out!"
               | notSayingAnything      = "Fine. Be that way!"
               | otherwise              = "Whatever."
 where
  questioning       = isQuestion xs
  yelling           = isYelling xs
  notSayingAnything = isNotSayingAnything xs
