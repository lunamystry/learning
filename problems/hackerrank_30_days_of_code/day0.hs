import System.IO

main :: IO ()
main = do
  welcome <- getLine
  putStrLn "Hello, world"
  putStrLn welcome
