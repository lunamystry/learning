import Data.List (group)

-- |This is a function.
fromDec :: [String] -> Int -> String
fromDec digits decNum
  | decNum < 0 = error "Don't know how to deal with negative numbers"
  | decNum < base = digits !! decNum
  | otherwise = fromDec digits (decNum `quot` base) ++ (show . rem decNum $ base)
  where
    base = length digits

toBin :: Int -> String
toBin = fromDec ["0", "1"]

main :: IO ()
main = interact $ show . maximum . map length . filter (\i -> '0' `notElem` i) . group . toBin . read
