process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();
});
function readLine() {
    return input_stdin_array[input_currentline++];
}


function fromDec(symbols, decimalNumber) {
  if (decimalNumber < 0) throw "Don't know how to deal with negative numbers";
  const base = symbols.length;
  if (decimalNumber < base) return symbols[decimalNumber]

  const remainder = decimalNumber % base;
  const quotient = ~~(decimalNumber/base);
  return fromDec(symbols, quotient) + remainder + ""
}


function toBinary(decimalNumber) {
  return fromDec(["0", "1"], decimalNumber);
}


function main() {
  const T=parseInt(readLine());
  console.log(Math.max(...(toBinary(T).split('0').map(i => i.length))))
}
