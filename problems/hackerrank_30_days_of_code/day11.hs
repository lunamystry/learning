type Row = [Int]
type Grid = [Row]

toInt :: String -> Int
toInt = read

hourglass :: Grid -> (Int , Int) -> [Int]
hourglass grid (r, c) = [ grid !! r !! c, grid !! r !! (c+1), grid !! r !! (c+2)
                        , grid !! (r+1) !! (c+1)
                        , grid !! (r+2) !! c, grid !! (r+2) !! (c+1), grid !! (r+2) !! (c+2)
                        ]

coordinates :: [(Int, Int)]
coordinates = [(r, c) | r <- [0..3], c <- [0..3]]

makeGrid :: String -> Grid
makeGrid = map (map toInt) . map words . lines

solve :: String -> String
solve i = show . maximum . map sum . map (hourglass $ makeGrid i) $ coordinates

main :: IO ()
main = interact $ solve
