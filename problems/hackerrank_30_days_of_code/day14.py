def abs_diff(a):
    return lambda b: abs(a - b)


class Difference:
    def __init__(self, a):
        self.__elements = a

        # Add your code here
        self.maximumDifference = 0

    def computeDifference(self):
        """finds the maximum absolute difference between any numbers in and
        stores it in the maximumDifference instance variable"""
        diffs = []
        for i, element in enumerate(self.__elements):
            diffs += diffs + \
                list(map(abs_diff(element), self.__elements[i + 1:]))
        self.maximumDifference = max(diffs)


# End of Difference class


if __name__ == '__main__':
    _ = input()
    a = [int(e) for e in input().split(' ')]

    d = Difference(a)
    d.computeDifference()

    print(d.maximumDifference)
