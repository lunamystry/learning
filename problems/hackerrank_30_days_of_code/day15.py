class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __str__(self):
        if self.next is None:
            return f"{self.data}"
        return f"{self.data} -> "


class Solution:
    def display(self, head):
        current = head
        while current:
            print(current, end=' ')
            current = current.next

    def insert(self, head, data):
        # Complete this method
        if head is None:
            head = Node(data)
        else:
            current = head
            previous = None
            while current:
                previous = current
                current = current.next
            previous.next = Node(data)

        return head


if __name__ == "__main__":
    mylist = Solution()
    T = int(input())
    head = None
    for i in range(T):
        data = int(input())
        head = mylist.insert(head, data)
    mylist.display(head)
