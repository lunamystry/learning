{-# LANGUAGE FlexibleInstances, UndecidableInstances, DuplicateRecordFields #-}

module Main where

-- Complete the solve function below.
solve :: Double -> Int -> Int -> IO ()
solve meal_cost tip_percent tax_percent = do
  print $ round $ meal_cost + cost tip_percent + cost tax_percent
    where
      cost p = fromIntegral(p)/100 * meal_cost

main :: IO()
main = do
    meal_cost <- readLn :: IO Double
    tip_percent <- readLn :: IO Int
    tax_percent <- readLn :: IO Int

    solve meal_cost tip_percent tax_percent
