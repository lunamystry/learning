data Result a = Result { swapCount :: Int
                       , sorted :: [a]
                       }
  deriving (Read, Eq)

instance (Show a) => Show (Result a) where
  show (Result c xs) = "Array is sorted in " <> show c <> " swaps.\n\
                       \First Element: " <> show (head xs) <> "\n\
                       \Last Element: " <> show (last xs) <> ""


-- original bubblesort from: https://codereview.stackexchange.com/questions/197868/bubble-sort-in-haskell
bubblesort :: Ord a => [a] -> Result a
bubblesort xs = foldr swapTill (Result 0 xs) [1..length xs - 1]

swapTill :: Ord a => Int -> Result a -> Result a
swapTill 0 = id
swapTill count = bubble
  where
    bubble :: Ord a => Result a -> Result a
    bubble (Result c (x:y:xs))
      | x > y = Result (swapCount swapped) (y: sorted swapped)
      | otherwise = Result (swapCount notswapped) (x: sorted notswapped)
      where
        swapped = swapTill (count - 1) (Result (c + 1) (x:xs))
        notswapped = swapTill (count - 1) (Result c (y:xs))

readInt :: String -> Int
readInt = read

main :: IO ()
main = interact $ show . bubblesort . map readInt . tail . words
