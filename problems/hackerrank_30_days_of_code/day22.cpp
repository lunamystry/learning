#include <iostream>
#include <cstddef>

using namespace std;

class Node {
public:
    int data;
    Node *left;
    Node *right;
    Node(int d) {
        data = d;
        left = NULL;
        right = NULL;
    }
};

class Solution {
public:
    Node* insert(Node* root, int data) {
        if(root == NULL) {
            return new Node(data);
        }
        else {
            Node* cur;
            if(data <= root->data) {
                cur = insert(root->left, data);
                root->left = cur;
            }
            else {
                cur = insert(root->right, data);
                root->right = cur;
            }

            return root;
        }
    }

    int getHeight(Node* root) {
        //Write your code here
        if (root == NULL) {
            return 0;
        }

        // Looks like it is added to make the math work. Every leaf get's a 1
        // added to it which is like for some ghost leafs it has, so you
        // basically remove them with the -1
        if (root->left == NULL && root->right == NULL) {
            return -1;
        }

        return 1 + max(getHeight(root->left),getHeight(root->right));
    }

    // This is in #include <algorithm>
    int max(int a, int b) {
        if (a > b) {
            return a;
        }
        return b;
    }

}; //End of Solution

int main() {
    Solution myTree;
    Node* root = NULL;
    int t;
    int data;

    cin >> t;

    while(t-- > 0) {
        cin >> data;
        root = myTree.insert(root, data);
    }
    int height = myTree.getHeight(root);
    cout << height;

    return 0;
}
