import Data.List

isFactorOf :: Int -> Int -> Bool
isFactorOf i n
  | i `mod` n == 0 = True
  | otherwise = False

isPrime :: Int -> String
isPrime 0 = "Not prime"
isPrime 1 = "Not prime"
isPrime 2 = "Prime"
isPrime n
  | n > 2 && n `mod` 2 == 0 = "Not prime"
  | otherwise = case find (isFactorOf n) [3,5.. end n] of
                      Just n -> "Not prime"
                      Nothing -> "Prime"
      where
        end = (+1) . floor . sqrt . fromIntegral

readInt :: String -> Int
readInt = read

main :: IO ()
main = interact $ unlines . map isPrime . map readInt . tail . lines
