data Date = Date { day :: Int
                 , month :: Int
                 , year :: Int
                 }
  deriving (Eq,Show)

instance Read Date where
  readsPrec _ = asDate. map read . words
    where
      asDate [d,m,y] = [(Date d m y, "")]
      asDate xs = []

instance Ord Date where
  Date d1 m1 y1 <= Date d2 m2 y2
    | y1 < y2 = True
    | y1 == y2 && m1 < m2 = True
    | y1 == y2 && m1 == m2 && d1 < d2 = True
    | y1 == y2 && m1 == m2 && d1 == d2 = True
    | otherwise = False


readDate :: String -> Date
readDate = read

calculateFine :: [Date] -> Int
calculateFine (ret:due:xs)
  | onOrBeforeDue = 0
  | lessThanAMonth = 15 *  abs (day ret - day due)
  | lessThanAYear = 500 * abs (month ret - month due)
  | otherwise = 10000
    where
      onOrBeforeDue = ret <= due
      lessThanAMonth = ret > due && month ret == month due && year ret == year due
      lessThanAYear = ret > due && year ret == year due

main :: IO ()
main = interact $ show . calculateFine . map readDate . lines
