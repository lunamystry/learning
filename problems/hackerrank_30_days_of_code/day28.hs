import Data.List

data Email = Email { emailFirstName :: String, emailAddress:: String }
  deriving (Show)

instance Read Email where
  readsPrec _ = asEmail . words
    where
      asEmail (x:y:xs) = [(Email x y, "")]
      asEmail xs = []

isGmail :: Email -> Bool
isGmail = isSuffixOf "@gmail.com" . emailAddress

readEmail :: String -> Email
readEmail = read


main :: IO ()
main = interact $ unlines . sort . map emailFirstName . filter isGmail . map readEmail . tail . lines
