import Data.List
import Data.Bits

flatten :: Int -> [[Int]] -> [Int]
flatten k arr = [y | x<- arr, y <- x, y < k]

combine :: (Int, [Int]) -> [Int]
combine (x,xs) = map (x.&.) xs

ands :: Int -> Int -> [Int]
ands k n = flatten k . zipWith (curry combine) [n - 1, n - 2..1] . tail . inits $ [n, n - 1 .. 2]

solve :: (Int, Int) -> Int
solve (n,k) = maximum $ ands k n

readInput :: String -> (Int, Int)
readInput i = (head inputs, last inputs)
  where
    inputs = map read . words $ i

main :: IO ()
main = interact $ unlines . map (show . solve . readInput) . tail . lines
