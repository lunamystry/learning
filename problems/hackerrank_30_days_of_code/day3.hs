import Data.Bits

isOdd :: Int -> Bool
isOdd n = n .&. 1 /= 0

fizzBuzz :: Int -> IO ()
fizzBuzz n
  | isOdd n = putStrLn "Weird"
  | n >= 2 && n <= 5 = putStrLn "Not Weird"
  | n >= 6 && n <= 20 = putStrLn "Weird"
  | n > 20 = putStrln "Not Weird"

main :: IO ()
main = do
  n <- readLn :: IO Int
  fizzBuzz n
