import Control.Monad

showMultiple :: Int -> Int -> String
showMultiple n howMany = show n ++ " x " ++ show howMany ++ " = " ++ show (n * howMany)

main :: IO ()
main = do
  n <- readLn :: IO Int
  mapM_ (putStrLn . showMultiple n) [1..10]
