import Control.Monad
import Data.Bits

data Group = Group { oddGroup :: String,
                    evenGroup :: String }
  deriving (Read, Eq)

instance Show Group where
  show (Group o e) = o <> " " <> e

group ::  Group -> (Char, Int) -> Group
group g i = if isOdd $ snd i then
                Group (oddGroup g <> [fst i]) (evenGroup g)
            else
                Group (oddGroup g) (evenGroup g <> [fst i])

isOdd :: Int -> Bool
isOdd n = n .&. 1 /= 0

readString :: IO String
readString = getLine

readInt :: IO Int
readInt = readLn

getWord :: IO ()
getWord = do
  s <- readString
  let g = foldl group (Group "" "") (zip s [1..])
  print g

process:: Int -> IO ()
process testCases = replicateM_ testCases getWord

main :: IO ()
main = readInt >>= process
