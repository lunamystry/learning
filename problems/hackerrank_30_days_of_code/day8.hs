import Prelude hiding (lookup)
import Control.Monad
import Data.Map (Map, insert, fromList, lookup)

type Phonebook = Map String String

makePhonebook :: Phonebook -> String -> Phonebook
makePhonebook p e = insert (head . words $ e) (last . words $ e) p

phonebook :: [String] -> Phonebook
phonebook xs = foldl makePhonebook (fromList []) xs

isIn :: Phonebook -> String -> String
isIn p query  = case lookup query p of
                  Just number -> query <> "=" <> number
                  Nothing -> "Not found"

query :: [String] -> Phonebook -> [String]
query qs p = map (isIn p) qs

parse :: [String] -> ([String], [String])
parse (x:xs) = (take n xs, drop n xs)
  where n = read x

solve :: ([String], [String]) -> [String]
solve (entries,queries) = query queries $ phonebook entries

main :: IO ()
main = interact $ unlines . solve . parse . lines
