factorial :: Int -> Int
factorial n
  | n <= 1 = 1
  | otherwise = (n*) $ factorial $ n - 1

main :: IO ()
main = interact $ show . factorial . read

-- Because interact seems to break ghci, here is a solution that doesn't

-- readInt :: IO Int
-- readInt = readLn

-- solve :: Int -> IO ()
-- solve n = print $ factorial n

-- main :: IO ()
-- main = readInt >>= solve
