const animal = {
  walk() {
    if (!this.isSleeping) {
      console.log("I walk");
    }
  },
  sleep() {
    this.isSleeping = true;
  },
  wakeup() {
    this.isSleeping = false;
  },
  jumps: null
}

const rabbit = {
  name: "While Rabbit!",
  jumps: true,
  __proto__: animal
}


console.log("\n --- isSleeping?");
// If you have not set the property on rabbit, it will come from animal, so
// animal will be controlled animal until you set the property
animal.sleep();
console.log(rabbit.isSleeping);
console.log(animal.isSleeping);
rabbit.wakeup();
console.log(rabbit.isSleeping);
console.log(animal.isSleeping);

console.log("\n --- propeties");
console.log(Object.keys(rabbit));
for(let p in rabbit) console.log(p);

console.log("\n --- delete a few things");
console.log(rabbit.jumps); // true
delete rabbit.jumps;
console.log(rabbit.jumps); // null
delete animal.jumps;
console.log(rabbit.jumps); // undefined
