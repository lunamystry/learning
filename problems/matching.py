#!/usr/bin/env python3

pairs = {
    ")": "(",
    "]": "[",
    "}": "{"
}

def isValid(s: str) -> bool:
    """
        :type s: str - String to be tested for validity
        :rtype: bool - Returns true if the string is valid else false
    """
    # add ( pop if ) add [ pop if] add { pop if }
    if len(s) == 0:
        # only valid is 1 and 2 are met
        return False;

    a = [s[0]]
    last_added = s[0]
    for i in s[1:]:
        if i in pairs.keys() and pairs[i] == last_added:
            a.pop()
        elif i in pairs.keys():
            # closing bracket that does not match the opening one
            return False
        elif i in pairs.values():
            a.append(i)
            last_added = i

    # should be empty by this point
    return len(a) == 0


if __name__ == "__main__":
    line = input()
    if isValid(line):
        print("valid")
    else:
        print("invalid")
