module Main where

readInteger :: String -> Integer
readInteger = read

count_a :: String -> Integer
count_a = fromIntegral . length . filter (\i -> i == 'a')

solve :: (String, Integer) -> Integer
solve (s, rt) = ((count_a s) * ((rt - eb) `div` lenStr)) + leftover
 where
  lenStr   = fromIntegral . length $ s
  eb       = mod rt lenStr
  leftover = count_a . take (fromIntegral eb) $ s


main :: IO ()
main = interact $ show . solve . parse . lines
 where
  parse :: [String] -> (String, Integer)
  parse (str : numStr : []) = (str, readInteger numStr)
  parse xs =
    error
      $  "Well this should not happen. Expected 2 inputs from stdin got: "
      <> show xs


-- THIRD ATTEMPT
-- readDouble :: String -> Double
-- readDouble = read

-- count_a :: String -> Double -> Integer
-- count_a str repeatTill = count
--  where
--   a_s    = fromIntegral . length . filter (\i -> i == 'a') $ str
--   strLen = fromIntegral . length $ str
--   c      = (a_s * repeatTill) / strLen
--   count | length str `mod` 2 == 0 = floor c
--         | otherwise               = ceiling c


-- solve :: [String] -> Integer
-- solve (str : numStr : []) = count_a str (readDouble numStr)
-- solve xs =
--   error
--     $  "Well this should not happen. Expected 2 inputs from stdin got: "
--     <> show xs


-- main :: IO ()
-- main = interact $ show . solve . lines


-- SECOND ATTEMPT
-- readInt :: String -> Int
-- readInt = read

-- count_a_s :: String -> Int -> Int -> Int -> Int
-- count_a_s str cursor countdown counter
--   | countdown <= 0       = counter
--   | str !! cursor == 'a' = count_a_s str newCursor (countdown - 1) (counter + 1)
--   | otherwise            = count_a_s str newCursor (countdown - 1) counter
--   where newCursor = mod (cursor + 1) (length str)

-- solve :: [String] -> Int
-- solve (str : numStr : []) = count_a_s str 0 (readInt numStr) 0
-- solve xs =
--   error
--     $  "Well this should not happen. Expected 2 inputs from stdin got: "
--     <> show xs

-- main :: IO ()
-- main = interact $ show . solve . lines


-- FIRST ATTEMPT
-- readInt :: String -> Int
-- readInt = read

-- solve :: [String] -> Int
-- solve (str : numStr : [])
--   | length str == 0
--   = 0
--   | length str == 1
--   = num
--   | otherwise
--   = length . filter (\i -> i == 'a') . take num . foldl (<>) "" $ replicate
--     num
--     str
--   where num = readInt numStr
-- solve xs =
--   error
--     $  "Well this should not happen. Expected 2 inputs from stdin got: "
--     <> show xs


-- main :: IO ()
-- main = interact $ show . solve . lines
