package me.mandla
package result

import java.{util => ju}

object result:
  // 1. Immutable internal state
  //    private constructor because I want my constructors to be used to reduce chance of
  //    internal scope inconsistency:
  //      if there are errors, data should be None, if there is Data, errors should be empty
  class Result[Data] private (data: Option[Data], errors: List[Error]):

    // 2. you need constructors for the internal state
    //    - It is not using Data, this would be a problem for map
    //    - the constructors should be static but I don't know how to do that in scala yet
    def success[SomeType](data: SomeType): Result[SomeType] =
      Result(Some(data), List())

    def failure[SomeType](errors: List[Error]): Result[SomeType] =
      Result(None, errors)

    def failure[SomeType](error: Error): Result[SomeType] =
      Result(None, List(error))

    // 3. ability to use and even "change" the immutable internal state
    def map[NewData](mapper: Data => NewData): Result[NewData] =
      // makes assumption about the internal consistency I touched on in the private constructor.
      if (data.isDefined)
        // allows manipulation of the data
        success(mapper(data.get))
      else
        // propagates the error if there is one
        failure(errors)

    // 4. Optional inspired .or()
    //    Usage of .or()
    //      cache.get(key)
    //           .or(service.get(key))
    //           .map(item -> item.name)
    //    The implementation does the map code again, cause there is a problem with supplier returning another Result
    def or(supplier: () => Result[Data]): Result[Data] =
      // if (data.isDefined)
      //   success(data.get)
      // else
      //   supplier()

      // 6. Refactor or to use flatMap
      flatMap(_ => supplier())

    // 5. flatMap can make writing or a little cleaner,
    //    but I am not smart enough to stay in the Result context in order to implement it, so 4 more methods are added
    def flatMap[NewData](mapper: Data => Result[NewData]): Result[NewData] =
      if (data.isDefined)
        // handle the mapper result
        val res = mapper(data.get)
        if (res.isSuccess())
          success(res.get()) // why does this need to be called with ()?
        else
          failure(res.getErrors)
      else
        // propergates the error if there is one
        failure(errors)

    def isSuccess(): Boolean =
      data.isDefined

    def isFailure(): Boolean =
      !errors.isEmpty

    // Inspired by Optional.get, an escape hatch basically which I guess should be avoided
    // This is the same as orElseThrow
    def get(): Data =
      if (data.isEmpty)
        throw ju.NoSuchElementException("No value present")
      else
        data.get

    def getErrors: List[Error] =
      return errors

    // 7. I wanted to log the data while I was deciding what to do with it (actually either)
    def tap(tapper: Data => Unit): Result[Data] =
      map(d => {
        tapper(d)
        d
      })

    // 8. I wanted to log the errors while I how to properly handle them (actually either)
    //    But it needs a map for errors, se
    def tapErrors(tapper: List[Error] => Unit): Result[Data] =
      handle(errors => {
        tapper(errors)
        errors
      })

    def handle(mapper: List[Error] => List[Error]): Result[Data] =
      // makes assumption about the internal consistency I touched on in the private constructor.
      if (data.isDefined)
        // propagates the success if there is one
        success(data.get)
      else
        // allows manipulation of the errors
        failure(mapper(errors))

    // 9. flatMap for errors
    def catchAll(mapper: List[Error] => Result[Data]): Result[Data] =
      if (data.isDefined)
        // propergates the success if there is one
        success(data.get)
      else
        val res = mapper(errors)
        if (res.isSuccess()) // you can swallow errors if you want
          success(res.get())
        else
          failure(res.getErrors)

    // Either has this why not
    def either(
        successHandler: Data => Unit,
        failureHandler: List[Error] => Unit
    ): Unit =
      tap(successHandler)
        .tapErrors(failureHandler)

    def filter(predicate: Data => Boolean): Result[Data] =
      flatMap(d => {
        val keep: Boolean = predicate(d)
        if (keep) {
          success(d)
        }

        // I have fixed ErrorCodes
        failure(result.Error("NOT_FOUND", "filtered out" + d.toString()))
      })

    // Just because Future has a similar function
    def combine(
        r2: Result[Data],
        combiner: (Data, Data) => Data
    ): Result[Data] =
      return flatMap(d1 => r2.map(d2 => combiner(d1, d2)))

  end Result // I like this

  class Error(val code: String, val description: String)

  // Notes:
  //   I'm writing out the types by name as an experiment for myself for type level programming, you're meant to read the types
  //   in Scala (maybe even Java 17) this would a sum type
  //   Scala types come after, this is... different from Java
  //   def is for defining functions
  //   object is basically a singleton that I'm using as a namespace cause of the Kingdom of nouns
  //
  //   All the functions are implemented using map/handle or flatMap/catchAll
  //   Now need to tell the story of how each of them are used in code
