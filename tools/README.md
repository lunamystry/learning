# Tools

This is a list of tools I would like to get to know.

# TODO

- [] React basics
- [] AngularJS basics
- [] Vue.js basics
- [] Node event loop
- [] Modern Javascript
- [] Haskell for end to end (Reflex-FRP?)
- [] Purescript
- [] Deploy to with nix-flakes
- [] Elisp from scratch
- [] org-mode
