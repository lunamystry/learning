#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

{- Author:     Jeff Newbern
   Maintainer: Jeff Newbern <jnewbern@nomaware.com>
   Modified by: Mandla Mbuli <mail@mandla.me>
   Time-stamp: <Mon Nov 10 11:58:21 2003>
   Modification datetime: <2019-06-15 18:23>
   License:    GPL
-}

{- DESCRIPTION

  Exercise 3 - List Monad

  This is basically to show the flexibility of using Monads.

-}

import           Control.Monad                            ( MonadPlus
                                                          , mzero
                                                          )
import           Control.Applicative                      ( (<|>) )
import           Data.Maybe                               ( maybeToList )

-- everything you need to know about sheep
data Sheep = Sheep
  { name::String
  , mother::Maybe Sheep
  , father::Maybe Sheep
  }

-- we show sheep by name
instance Show Sheep where
  show s = show (name s)

paternalGrandfather :: Sheep -> Maybe Sheep
paternalGrandfather s = (return s) >>= father >>= father

paternalGrandmother :: Sheep -> Maybe Sheep
paternalGrandmother s = (return s) >>= father >>= mother

maternalGrandfather :: Sheep -> Maybe Sheep
maternalGrandfather s = (return s) >>= mother >>= father

maternalGrandmother :: Sheep -> Maybe Sheep
maternalGrandmother s = (return s) >>= mother >>= mother

fathersMaternalGrandmother :: Sheep -> Maybe Sheep
fathersMaternalGrandmother s = (return s) >>= father >>= mother >>= mother

mothersPaternalGrandfather :: Sheep -> Maybe Sheep
mothersPaternalGrandfather s = (return s) >>= mother >>= father >>= father

toMonad :: MonadPlus m => Maybe a -> m a
toMonad Nothing  = mzero
toMonad (Just s) = return s

parent :: MonadPlus m => Sheep -> m Sheep
parent s = (toMonad $ mother s) <|> (toMonad $ father s)

grandParent :: MonadPlus m => Sheep -> m Sheep
grandParent s =
  (toMonad $ maternalGrandmother s)
    <|> (toMonad $ maternalGrandfather s)
    <|> (toMonad $ paternalGrandmother s)
    <|> (toMonad $ paternalGrandfather s)

-- this builds our sheep family tree
breedSheep :: Sheep
breedSheep =
  let adam   = Sheep "Adam" Nothing Nothing
      eve    = Sheep "Eve" Nothing Nothing
      uranus = Sheep "Uranus" Nothing Nothing
      gaea   = Sheep "Gaea" Nothing Nothing
      kronos = Sheep "Kronos" (Just gaea) (Just uranus)
      holly  = Sheep "Holly" (Just eve) (Just adam)
      roger  = Sheep "Roger" (Just eve) (Just kronos)
      molly  = Sheep "Molly" (Just holly) (Just roger)
  in  Sheep "Dolly" (Just molly) Nothing

-- print Dolly's maternal grandfather
main :: IO ()
main =
  let dolly = breedSheep
  in  do
        print (parent dolly :: [Sheep])
        print (grandParent dolly :: [] Sheep)
        print (parent dolly :: Maybe Sheep)
        print (grandParent dolly :: Maybe Sheep)
