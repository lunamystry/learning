import Data.Time.Clock
import Data.Time.Calendar

exerciseOne :: Int -> String
exerciseOne 1 = "This sentence is false"
exerciseOne 2 = "No it is not"
exerciseOne _ = "One plus two is not seven"

exerciseTwo :: Int
exerciseTwo = 20938

exerciseThree :: IO (Integer, Int, Int)
exerciseThree = getCurrentTime >>= return . toGregorian . utctDay

main :: IO ()
main = do
    putStr "Exercise: "
    exercise <- read <$> getLine
    case exercise of
        1 ->
            do
                putStr "Option: "
                option <- read <$> getLine
                print $ exerciseOne option
        2 ->
            print $ show exerciseTwo
        3 ->
            do
                (_, _, date) <- exerciseThree
                print $ show date
        _ ->
            putStrLn "If that's valid exercise, I haven't done it yet"
