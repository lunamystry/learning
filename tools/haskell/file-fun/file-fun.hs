#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

{-
  Author:      Josh Clayton
  Copied from: https://github.com/joshuaclayton/file-fun

  Modified by: Mandla Mbuli <mail@mandla.me>

  Copyright (c) 2016 Josh Clayton

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
  of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
-}

{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}

module Main where

import qualified Control.Exception             as E
import qualified Data.Bifunctor                as BF
import qualified Data.Bool                     as B
import qualified Data.Char                     as C
import           Options.Applicative

-- types

data Options = Options
    { makeCapitalized :: Bool
    , makeExcited :: Bool
    , fromStdin :: Bool
    , fromFile :: Maybe String
    }

-- program

main :: IO ()
main = parseCLI >>= runProgram

runProgram :: Options -> IO ()
runProgram o =
  (handleExcitedness o . handleCapitalization o <$> getSource o) >>= putStr

-- data retrieval and transformation

getSource :: Options -> IO String
getSource o =
  B.bool (either id id <$> loadContents o) getContents $ fromStdin o

handleCapitalization :: Options -> String -> String
handleCapitalization o = B.bool id (map C.toUpper) $ makeCapitalized o

handleExcitedness :: Options -> String -> String
handleExcitedness o = B.bool id ("ZOMG " ++) $ makeExcited o

loadContents :: Options -> IO (Either String String)
loadContents o = maybe defaultResponse readFileFromOptions $ fromFile o
 where
  readFileFromOptions f = BF.first show <$> safeReadFile f
  defaultResponse = return $ Right "This is fun!"

-- CLI parsing

parseCLI :: IO Options
parseCLI = execParser (withInfo parseOptions "File Fun")
  where withInfo opts h = info (helper <*> opts) $ header h

parseOptions :: Parser Options
parseOptions =
  Options
    <$> (switch $ long "capitalize")
    <*> (switch $ long "excited")
    <*> (switch $ long "stdin")
    <*> (optional $ strOption $ long "file")

-- safer reading of files

safeReadFile :: FilePath -> IO (Either E.IOException String)
safeReadFile = E.try . readFile
