#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

summ :: IO ()
summ = interact $ show . sum . map read . tail . words

round5 :: Int -> Int
rount5 x
  | x >= 38 && (m5 - x) < 3 = m5
  | otherwise = x
  where
    m5 = x + (5 - x `mod` 5)

solve :: [Int] -> [Int]
solve xs = map round5 xs

main :: IO()
main = interact $ unlines . map show . solve . map read . tail . words
