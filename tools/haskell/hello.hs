import Data.Time.Clock.POSIX
import qualified Data.List as L

leo :: String
leo = "Leonard"

greet :: String -> IO ()
greet "Leonard" = putStrLn "Sawubona Mnumzan'"
greet name = putStrLn ("Hello from Haskell, " ++ name)

printSix :: IO ()
printSix = print (1 + 5)

printCurrentTime :: IO ()
printCurrentTime = getPOSIXTime >>= print

fun :: Bool -> Bool
fun = (\x -> True)

lengthOfThing :: Show a => a -> Int
lengthOfThing n = length $ show n

message :: String -> String
message name =
  case name of
    "Dave"  -> "I can't do that."
    "Sam"   -> "Play it again."
    "Creep" -> "Feeling lucky?"
    _       -> "Hello."

message' :: String -> String
message' "Dave" = "I can't do that."
message' "Sam"  = "Play it again."
message' _      = "Hello."

message'' :: String -> String
message'' name
  | name == "Dave" = "I can't do that."
  | name == "Sam"  = "Play it again."
  | otherwise      = "Hello."

shoppingList :: [String]
shoppingList =
  [ "Carrots"
  , "Oats"
  , "Butter"
  , "Apples"
  , "Milk"
  , "Cereal"
  , "Chocolate"
  , "Bananas"
  , "Broccoli"
  ]

joinedWithCommas :: [String] -> String
joinedWithCommas []     = ""
joinedWithCommas [x]    = x
joinedWithCommas (x:xs) = x ++ ", " ++ joinedWithCommas xs

commaed :: String
commaed = "There are " ++ (show $ length shoppingList)
          ++ " items on the shopping list."
          ++ " and the list is: "
          ++ joinedWithCommas shoppingList

movies =
  [ "Aeon Flux"
  , "The Black Cat"
  , "Superman"
  , "Stick It"
  , "The Matrix Revolutions"
  , "The Raven"
  , "Inception"
  , "Looper"
  , "Hoodwinked"
  , "Tell-Tale"
  ]

isGood :: String -> Bool
isGood (x:_) = x <= 'M'
isGood _     = False

assess :: String -> String
assess movie = movie ++ " - " ++ assessment
  where assessment = if isGood movie
                     then "Good"
                     else "Bad"

assessMovies :: [String] -> [String]
assessMovies = map assess

assessedMovies :: String
assessedMovies = L.intercalate "\n" $ assessMovies movies

type Name = String
type PriceInCents = Int
type ShoppingListItem = (Name, PriceInCents)
type ShoppingList = [] ShoppingListItem

shoppingList' :: ShoppingList
shoppingList' = [ ("Bananas", 300)
                , ("Chocolate", 250)
                , ("Milk", 300)
                , ("Apples", 450)
                ]

sumShoppingList :: ShoppingList -> PriceInCents
sumShoppingList []     = 0
sumShoppingList (x:xs) = snd x + sumShoppingList xs

sumShoppingList' :: ShoppingList -> PriceInCents
sumShoppingList' xs = foldr getPriceAndAdd 0 xs

getPriceAndAdd :: ShoppingListItem -> PriceInCents -> PriceInCents
getPriceAndAdd item currentTotal = snd item + currentTotal

shoppingListTotalPriceAsRands :: Int
shoppingListTotalPriceAsRands = div (sumShoppingList' shoppingList') 100

shoppingList'TotalPrice :: String
shoppingList'TotalPrice = "Price of shopping list is R"
                        ++ show shoppingListTotalPriceAsRands

main :: IO ()
main = putStrLn shoppingList'TotalPrice
