#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

import           Control.Monad

f :: Monad m => Integer -> m String
f a = return $ show a

g :: Monad m => String -> m Bool
g = return . (> 5) . length

h :: Monad m => Integer -> m Bool
h = f >=> g

main :: IO ()
main = do
  print (h 432 :: Maybe Bool)
  print (h 432 :: [] Bool)
  print (h 432 :: Either String Bool)
