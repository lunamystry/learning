addOneTo x = x + 1

-- Using let to define an inner function
alwaysEven a b = let isEven x = if even x
                                   then x
                                   else x - 1
                                in (isEven a, isEven b)

-- Using the where
alwaysEven' a b = (isEven a, isEven b)
    where isEven x = if even x
                        then x
                        else x - 1

-- Infix function
a +! b = a + 2*b


-------------------------------------------------------------------------------
-- Episode 2 - Lists
--

same = ['l', 'c'] == "lc"
errornousList = [1, 2, 3, error "Whoops", 5, 6, 7]
try list = take 3 errornousList

rangedList = [1..10]
infiList = [1,2..] -- in jumps of two
-- take 3 (drop 5 [1..])

person = (22, "Leonard")
-- fst person  -- > 22
-- snd person  -- > Leonard

people = [(26, "Leonard"), (25, "Katlego"), (30, "Amanda")]
-- lookup 25 people -- compares using the first type


-------------------------------------------------------------------------------
-- Episode 3 - More on Functions
--

command "greet"  = "Greeting"
command "depart" = "Farewell"
command _        = "are you coming or going?"

trd (_, _, a) = a

-- recursion
dropWhile'1 _ [] = []
dropWhile'1 bool (h:t) = -- break the list into head x  and tail xs
    if bool h then t else (h:t)

dropWhile'2 _ [] = []
dropWhile'2 bool xxs@(h:t) =
    if bool h then dropWhile'2 bool t else xxs

-- More recursion
map' _ [] = []
map' f (x:xs) = f x : map' f xs -- keep prepending to an empty list

-- Guards -- these don't check exact things, you can check with bool
last5 xxs@(_:xs)
  | length xxs <= 5 = xxs -- if the length of the list is less than 5, done
  | otherwise       = last5 xs -- otherwise, try again 

-------------------------------------------------------------------------------
-- Episode 4 -
-- Type Signatures

headInt :: [Maybe Int] -> Maybe Int
headInt [] = Nothing
headInt (x:_) = x

-------------------------------------------------------------------------------
-- Episode 5 -
--

