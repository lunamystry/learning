#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MonadFailDesugaring #-}
{-# LANGUAGE QuasiQuotes #-}

import Data.Text
import Data.Aeson (Value)
import Data.Aeson.QQ (aesonQQ)
import Data.Aeson.Lens
import Control.Lens

data User = User
  { _name     :: Text
  , _userId   :: Int
  , _metadata :: UserInfo
  }
  deriving (Show)

data UserInfo = UserInfo
  { _numLogins     :: Int
  , _associatedIps :: [Text]
  }
  deriving (Show)

makeLenses ''User
makeLenses ''UserInfo

user1 = User
  { _name = "qiao.yifan"
  , _userId = 101
  , _metadata = UserInfo
    { _numLogins = 10
    , _associatedIps =
      [ "53", "43"]
    }
  }

user2 = [aesonQQ|
  {
    "name": "qiao.yifan",
    "email": "qyifan@xingxin.com"
  }
|]

user3 = [aesonQQ|
  {
    "name": "ye.xiu",
    "metadata": {
      "num_logins": 27
    }
  }
|]

main :: IO ()
main = do
  putStrLn "I. "
  print $ user1 ^. name
  print $ user1 ^. metadata . numLogins
  print $ user1 & metadata . numLogins .~ 0
  print $ user1 & metadata . associatedIps %~ ("121":)
  print $ metadata . numLogins %~ (+ 1) $ user1
  putStrLn "\nII. "
  -- user1 & email .~ "qyifan@xingxin.com"  -- Cause there is no email lens
  print $ user1 & metadata .~ (UserInfo 17 [])
  print $ userId .~ -1 $ user1  -- was the I in userId suppose to be small?
  -- metadata . associatedIps .~ ["50"] & user1  -- Cause the & is on the wrong side, the lenses are compose to a function
  -- user1 ^. numLogins . metadata  -- Cause the order is wrong
  putStrLn "\nIII. "
