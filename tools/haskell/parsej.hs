#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}

module Main
  ( main
  )
where

import           Control.Applicative
import           Data.List                                ( intercalate )
import           Data.Set                                 ( Set )
import           Data.Text                                ( Text )
import           Data.Void
import           Text.Megaparsec                   hiding ( some )
import           Text.Megaparsec.Char
import qualified Data.Set                      as Set

data Custom
  = TrivialWithLocation [String] (Maybe (ErrorItem (Token Char))) (Set (ErrorItem Char))
  | FancyWithLocation [String] (ErrorFancy Void)
  deriving (Eq, Ord, Show)

instance ShowErrorComponent Custom where
  showErrorComponent (TrivialWithLocation stack us es) =
    parseErrorTextPretty (TrivialError @Char @Void undefined us es) ++ showPosStack stack
  showErrorComponent (FancyWithLocation stack cs) =
    showErrorComponent cs ++ showPosStack stack

showPosStack :: [String] -> String
showPosStack = intercalate ", " . fmap ("in " ++)

type Parser = Parsec Custom Text

inside :: String -> Parser a -> Parser a
inside location p = do
  r <- observing p
  case r of
    Left (TrivialError _ us es) ->
      fancyFailure . Set.singleton . ErrorCustom $ TrivialWithLocation
        [location]
        us
        es
    Left (FancyError _ xs) -> do
      let
        f (ErrorFail msg) =
          ErrorCustom $ FancyWithLocation [location] (ErrorFail msg)
        f (ErrorIndentation ord rlvl alvl) = ErrorCustom
          $ FancyWithLocation [location] (ErrorIndentation ord rlvl alvl)
        f (ErrorCustom (TrivialWithLocation ps us es)) =
          ErrorCustom $ TrivialWithLocation (location : ps) us es
        f (ErrorCustom (FancyWithLocation ps cs)) =
          ErrorCustom $ FancyWithLocation (location : ps) cs
      fancyFailure (Set.map f xs)
    Right x -> return x

myParser :: Parser String
myParser = some (char 'a') *> some (char 'b')

main :: IO ()
main = do
  parseTest (inside "foo" myParser)                "aaacc"
  parseTest (inside "foo" $ inside "bar" myParser) "aaacc"
