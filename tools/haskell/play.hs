#!/usr/bin/env stack
-- stack --resolver ghc-7.10.3 runghc

-- double :: Int -> Int
-- double x = x + x

-- square :: Int -> Int
-- square x = x * x

-- main :: IO ()
-- main = print $ map ($ 5) [double, square]
import Data.Monoid ((<>))

main :: IO ()
main = do
    putStrLn "Enter your year of birth"
    year <- read <$> getLine
    let age :: Int
        age = 2020 - year
    putStrLn $ "Age in 2020: " <> show age
