#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

import           Control.Monad.Reader

-- The Reader/IO combined monad, where Reader stores a string.
printReaderContent :: ReaderT String IO ()
printReaderContent = do
  content <- ask
  liftIO $ putStrLn ("The Reader Content: " ++ content)

main :: IO ()
main = do
  runReaderT printReaderContent "Some Content"
