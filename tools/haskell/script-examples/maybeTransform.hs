#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

import           Control.Applicative (Alternative, empty, (<|>))
import           Control.Monad       (MonadPlus, ap, liftM, mplus, msum, mzero)
import           Control.Monad.Trans (MonadTrans, lift)

newtype MaybeT m a = MaybeT {runMaybeT :: m (Maybe a)}

instance (Monad m) => Monad (MaybeT m) where
  return  = MaybeT . return . Just
  x >>= f = MaybeT $ do
    v <- runMaybeT x
    case v of
      Nothing -> return Nothing
      Just y  ->  runMaybeT (f y)

instance Monad m => Applicative (MaybeT m) where
    pure = return
    (<*>) = ap

instance Monad m => Functor (MaybeT m) where
    fmap = liftM

instance Monad m => Alternative (MaybeT m) where
    empty   = MaybeT $ return Nothing
    x <|> y = MaybeT $ do maybe_value <- runMaybeT x
                          case maybe_value of
                               Nothing -> runMaybeT y
                               Just _  -> return maybe_value

instance Monad m => MonadPlus (MaybeT m) where
    mzero = empty
    mplus = (<|>)

instance MonadTrans MaybeT where
    lift = MaybeT . (liftM Just)

readUserName :: MaybeT IO String
readUserName = prompt "username" (\x -> length x > 3)

readEmail :: MaybeT IO String
readEmail = prompt "email" (\x -> length x > 2)

readPassword :: MaybeT IO String
readPassword = prompt "password" (\x -> length x > 8)

prompt :: String -> (String -> Bool) -> MaybeT IO String
prompt p isValid = msum $ repeat $ MaybeT $ do
  putStr $ p <> " > "
  str <- getLine
  if isValid str then return $ Just str else return Nothing

login :: String -> String -> String -> IO ()
login u e p = print $ u <> " " <> e <> " " <> p

main :: IO ()
main = do
  maybeCreds <- runMaybeT $ do
    usr      <- readUserName
    email    <- readEmail
    password <- readPassword
    return (usr, email, password)
  case maybeCreds of
    Nothing        -> print "Something seems to be wrong with the credentials"
    Just (u, e, p) -> login u e p
