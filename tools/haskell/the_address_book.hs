import qualified Data.List as L

type Name = String
type Year = Int
data Person = Person
    { personFirstName :: Name
    , personLastName :: Name
    , yearOfBirth :: Year }

instance Show Person where
    show (Person first last year) = first ++ " " ++ last ++ " " ++ show year

-- the famous mathematician
-- Blaise Pascal
blaise :: Person
blaise = Person "Blaise" "Pascal" 1623

blaiseRecord :: Person
blaiseRecord =
  Person { personFirstName = "Blaise"
         , personLastName = "Pascal"
         , yearOfBirth = 1623 }

traise :: Person
traise = blaise { personFirstName = "Traise" }

people :: [Person]
people =
  [ Person "Isaac" "Newton" 1643
  , Person "Leonard" "Euler" 1707
  , Person "Blaise" "Pascal" 1623
  , Person "Ada" "Lovelace" 1815
  , Person "Alan" "Turing" 1912
  , Person "Haskell" "Curry" 1900
  , Person "John" "von Neumann" 1903
  , Person "Lipot" "Fejer" 1880
  , Person "Grace" "Hopper" 1906
  , Person "Anita" "Borg" 1949
  , Person "Karen" "Sparck Jones" 1935
  , Person "Henriette" "Avram" 1919 ]

firstAfter1900 :: Maybe Person
firstAfter1900 =
    L.find (\(Person _ _ year) -> year >= 1900) people

firstAfter1900' :: Maybe Person
firstAfter1900' =
    L.find (\person -> yearOfBirth person >= 1900) people

firstNameBeginsWithL :: Person -> Bool
firstNameBeginsWithL p =
  case personFirstName p of
    'L':_ -> True
    _     -> False

makeNewListWithOnlyLPeople :: [Person] -> [Person]
makeNewListWithOnlyLPeople [] = []
makeNewListWithOnlyLPeople (x:xs)
  | firstNameBeginsWithL x =
      x : makeNewListWithOnlyLPeople xs
  | otherwise =
      makeNewListWithOnlyLPeople xs

peopleThatBeginWithL =
  makeNewListWithOnlyLPeople people

makeNewListWithOnlyLPeople' :: [Person] -> [Person]
makeNewListWithOnlyLPeople' xs =
  filter firstNameBeginsWithL xs

makeNewListWithOnlyLPeople'' :: [Person] -> [Person]
makeNewListWithOnlyLPeople'' xs =
  filter firstNameBeginsWithL xs

-- don't get confused, c is not the letter c here
-- it's a variable name, holding the Char value
-- we're matching on
firstLetterIs :: Char -> String -> Bool
firstLetterIs c ""    = False
firstLetterIs c (x:_) = c == x

firstNameBeginsWith :: Char -> Person -> Bool
firstNameBeginsWith c p =
    firstLetterIs c firstName
  where firstName = personFirstName p

peopleThatBeginWith :: Char -> [Person]
peopleThatBeginWith l =
  filter (firstNameBeginsWith l) people

main :: IO ()
main = do
    putStr "Exercise: "
    exercise <- read <$> getLine
    case exercise of
        1 ->
            do
                print "Without record syntax: "
                print blaise
        2 ->
            do
                print "With record syntax: "
                print blaiseRecord
        3 ->
            do
                print "Traise"
                print traise
        4 ->
            do
                print "People"
                print people
        5 ->
            do
                print "First after 1900"
                print firstAfter1900
        6 ->
            do
                print "First after 1900 (using record field name)"
                print firstAfter1900'
        7 ->
            do
                print "People with names beginning with L"
                print peopleThatBeginWithL
        8 ->
            do
                print "People with names beginning with L (using filter)"
                print $ makeNewListWithOnlyLPeople' people
        9 ->
            do
                print "People with names beginning with L (using filter no args)"
                print $ makeNewListWithOnlyLPeople'' people
        10 ->
            do
                print "People with names beginning with L (using filter eta reduction)"
                print "Enter letter: "
                letter <- getChar
                print $ peopleThatBeginWith letter
        _ ->
            print "Unknown option"
