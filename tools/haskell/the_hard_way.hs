import Data.List (foldl')
import System.Environment (getArgs)
import Data.Char

main :: IO ()
main = do
    args <- getArgs
    print args

evenSum :: Integral a => [a] -> a
evenSum = foldl' (+) 0 . filter even
