type Name = String
type Year = Int
data Person = Person {
    firstName :: Name,
    lastName :: Name,
    birthYear :: Year
    } deriving (Show)

mandla :: Person
mandla = Person "Mandla" "Mbuli" 1988

leonard :: Person
leonard = Person {
    firstName = "Leonard",
    lastName = "Mbuli",
    birthYear = 1988
    }

pelonomi :: Person
pelonomi = Person "Pelonomi" "Mogotsi" 1988

marriedPelo :: Person
marriedPelo = pelonomi { lastName = "Mbuli" }

people :: [Person]
people =
  [ Person "Isaac" "Newton" 1643
  , Person "Leonard" "Euler" 1707
  , Person "Blaise" "Pascal" 1623
  , Person "Ada" "Lovelace" 1815
  , Person "Alan" "Turing" 1912
  , Person "Haskell" "Curry" 1900
  , Person "John" "von Neumann" 1903
  , Person "Lipot" "Fejer" 1880
  , Person "Grace" "Hopper" 1906
  , Person "Anita" "Borg" 1949
  , Person "Karen" "Sparck Jones" 1935
  , Person "Henriette" "Avram" 1919 ]

main :: IO ()
main = print marriedPelo
