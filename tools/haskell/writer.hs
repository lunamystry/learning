#!/usr/bin/env stack
-- stack --resolver lts-13.19 script

import           Control.Monad
import           Control.Monad.Trans.Writer.Strict

data LogEntry = LogEntry { msg::String }
  deriving (Eq, Show)

calc :: Writer [LogEntry] Integer
calc = do
  output "start"
  let x = sum [1 .. 10000000]
  output (show x)
  output "done"
  return x

output :: String -> Writer [LogEntry] ()
output x = tell [LogEntry x]

main :: IO ()
main = mapM_ print $ execWriter calc
