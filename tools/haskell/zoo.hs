import qualified Data.List as L

data Animal = Giraffe
            | Elephant
            | Tiger
            | Flea

type Zoo = [Animal]

localZoo :: Zoo
localZoo = [ Elephant
           , Tiger
           , Tiger
           , Giraffe
           , Elephant
           ]

adviceOnEscape :: Animal -> String
adviceOnEscape animal =
  case animal of
    Giraffe   -> "Look up"
    Elephant  -> "Ear to the ground"
    Tiger     -> "Check the morgues"
    Flea      -> "Don't worry"

adviceOnEscape' :: Animal -> String
adviceOnEscape' Giraffe  = "Look up"
adviceOnEscape' Elephant = "Ear to the ground"
adviceOnEscape' Tiger    = "Check the morgues"
adviceOnEscape' Flea     = "Don't worry"

adviceOnZooEscape :: Zoo -> [String]
adviceOnZooEscape = map adviceOnEscape'

main :: IO ()
main = putStrLn stringToPrint
  where
    stringToPrint = L.intercalate ", " advices
    advices = adviceOnZooEscape localZoo
