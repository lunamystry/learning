from typing import Optional

import fastapi
import uvicorn

api = fastapi.FastAPI()


@api.get('/')
def calculate(x: int, y: int, z: Optional[int]):
    value = (y + z) * x
    return {
        "result": value
    }


uvicorn.run(api)
