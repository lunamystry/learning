import json
from pathlib import Path

import fastapi
import uvicorn
from pydantic import ValidationError
from starlette.staticfiles import StaticFiles

from home import router as home_router
from weather import openweather
from weather import router as weather_router


def configure():
    api = fastapi.FastAPI()
    api = configure_api_keys(api)
    api = configure_routing(api)
    return api


def configure_api_keys(api: fastapi.FastAPI):
    file = Path('settings.json').absolute()
    if not file.exists():
        print(f"ERROR: {file} file not found. See settings_template.json")
        raise RuntimeError(f"{file} file not found")

    with file.open('r') as fin:
        settings = json.load(fin)
        openweather.API_KEY = settings.get("api_key")

    return api


def configure_routing(api: fastapi.FastAPI):
    api.mount('/static', StaticFiles(directory='static'), name='static')
    api.include_router(home_router)
    api.include_router(prefix='/api', router=weather_router)
    return api


# when run in production with: uvicorn main:api
api = configure()


@api.exception_handler(ValidationError)
async def validation_error_handler(req: fastapi.Request, exc: ValidationError):
    return fastapi.responses.JSONResponse(
        status_code=400,
        content=json.loads(exc.json())
    )

if __name__ == "__main__":
    uvicorn.run(api)
