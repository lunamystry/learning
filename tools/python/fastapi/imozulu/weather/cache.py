import datetime
from typing import Optional, Tuple

from .location import Location
from .units import Units

__cache = {}
lifetime_in_hours = 1.0


def get_weather(loc: Location, units: Units) -> Optional[dict]:
    key = __create_key(loc, units)
    data: dict = __cache.get(key)
    if not data:
        return None

    last = data['time']
    dt = datetime.datetime.now() - last
    if dt / datetime.timedelta(minutes=60) < lifetime_in_hours:
        return data['value']

    del __cache[key]
    return None


def set_weather(loc: Location, units: Units, value: dict):
    key = __create_key(loc, units)
    data = {
        'time': datetime.datetime.now(),
        'value': value
    }
    __cache[key] = data
    __clean_out_of_date()


def __create_key(loc: Location, units: Units) -> Tuple[str, str, str, str]:
    if not loc.city or not loc.country or not units:
        raise Exception("City, country, and units are required")

    return (loc.city,
            loc.province,
            loc.country,
            units.units)


def __clean_out_of_date():
    for key, data in list(__cache.items()):
        dt = datetime.datetime.now() - data.get('time')
        if dt / datetime.timedelta(minutes=60) > lifetime_in_hours:
            del __cache[key]
