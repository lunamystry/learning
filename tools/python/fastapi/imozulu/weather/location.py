from typing import Optional

from pydantic import BaseModel, validator


class Location(BaseModel):
    city: str
    # order matters for validation, country has to come before province
    country: Optional[str] = 'ZA'
    province: Optional[str] = ''

    @validator('city')
    def make_city_lowercase(cls, v):
        return v.lower().strip()

    # TODO: thought I was being smart using province. This has to be state.
    @validator('province')
    def stat_must_be_2_letters(cls, v, values, **kwargs):
        if 'country' not in values:
            raise ValueError(f'to use province "{v}" country has to be "us"')

        if values['country'].lower().strip() != 'us':
            raise ValueError(
                f'to use province "{v}" country has to be "us", not {values["country"]}')

        if len(v) != 2:
            raise ValueError(
                (f'Invalid state: {v}.'
                 ' It must be a two letter abbreviation such as CA or KS'
                 ' (use for US only).'))

        return v.lower().strip()

    @ validator('country')
    def country_must_be_2_letters(cls, v):
        if len(v) != 2:
            raise ValueError('country must be 2 letters e.g: za')

        return v.lower().strip()
