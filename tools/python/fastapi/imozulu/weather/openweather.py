from typing import Optional

import httpx
from errors import StatusError
from httpx import Response

from . import cache
from .location import Location
from .units import Units

API_KEY: Optional[str] = None  # read from settings.json at runtime


def make_query(loc: Location):
    if loc.province:
        query = f"{loc.city},{loc.province},{loc.country}"
    else:
        query = f"{loc.city},{loc.country}"
    return query


async def get_report(loc: Location, units: Units):
    if forecast := cache.get_weather(loc, units):
        return forecast

    query = make_query(loc)
    base_url = "https://api.openweathermap.org/data/2.5/weather"
    url = f"{base_url}?q={query}&appid={API_KEY}&units={units}"

    print(f'getting weather with: {url}')
    async with httpx.AsyncClient() as client:
        resp: Response = await client.get(url)
        if resp.status_code != 200:
            raise StatusError(resp.text, status_code=resp.status_code)

    data = resp.json()
    forecast = data['main']

    cache.set_weather(loc, units, forecast)

    return forecast
