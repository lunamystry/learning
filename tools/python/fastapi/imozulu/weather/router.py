import json

from errors import StatusError
from fastapi import APIRouter, Depends, responses

from .location import Location
from .openweather import get_report
from .units import Units

router = APIRouter()


@router.get('/weather/{city}')
async def weather(location: Location = Depends(), units: Units = Depends()):
    try:
        return await get_report(location, units)
    except StatusError as se:
        return responses.JSONResponse(json.loads(se.error_msg), status_code=se.status_code)
    except Exception as x:
        return responses.JSONResponse(content=str(x), status_code=500)
