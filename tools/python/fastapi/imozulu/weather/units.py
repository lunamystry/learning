from typing import Optional

from pydantic import BaseModel, validator


class Units(BaseModel):
    units: Optional[str] = 'metric'
    _valid_units = {'standard', 'metric', 'imperial'}

    @validator('units')
    def units_must_known_value(cls, v):
        if v not in cls._valid_units:
            error = f"Invalid units '{v}', it must be one of {cls._valid_units}."
            raise ValueError(error)

        return v.lower().strip()
