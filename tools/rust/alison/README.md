# Alison is into Rust now

## Development (Flakes)

This repo uses [Flakes](https://nixos.wiki/wiki/Flakes) from the get-go, but
compat is provided for traditional nix-shell/nix-build as well (see the section
below).

```bash
# Dev shell
nix develop

# or just run directly
nix run

# or run via cargo
nix develop -c cargo run

# build
nix build
```

There is a also a `scripts/run` script which starts 'cargo watch'

Built from [Nix-ifying Rust projects](https://notes.srid.ca/rust-nix).
