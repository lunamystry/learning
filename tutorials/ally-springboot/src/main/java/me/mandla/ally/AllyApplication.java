package me.mandla.ally;

import java.security.Principal;
import java.util.Map;
import java.util.UUID;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api")
public class AllyApplication {

  @RequestMapping("/resource")
  public Map<String, Object> home() {
    Map<String, Object> model = Map.of("id", UUID.randomUUID().toString(), "content", "Hello World");

    return model;
  }

  @RequestMapping("/user")
  public Principal user(Principal user) {
    return user;
  }

  public static void main(String[] args) {
    SpringApplication.run(AllyApplication.class, args);
  }
}
