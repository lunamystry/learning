package me.mandla.ally.question;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class Question {
  private final Integer questionId;
  private final String question;
  private final String answer;

  public Question(Integer id, String q, String a) {
    this.questionId = id;
    this.question = q;
    this.answer = a;
  }

  public Integer getQuestionId() {
    return questionId;
  }

  public String getQuestion() {
    return question;
  }

  public String getAnswer() {
    return answer;
  }
}
