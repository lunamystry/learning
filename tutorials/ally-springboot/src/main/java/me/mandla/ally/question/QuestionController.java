package me.mandla.ally.question;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/questions")
public class QuestionController {
    private static final List<Question> QUESTIONS = Arrays.asList(
            new Question(1, "What is Bitwarden?", "An opensource password manager"),
            new Question(2, "Why should I care?",
                    "Passwords are hard to remember, this makes it easier and thus more secure"),
            new Question(3, "One password? That sounds dangerous! What if I lose it?",
                    "Well, the is the advantage and disdvantage of a password manager"));

    @GetMapping(path = "")
    public List<Question> getQuestions() {
        return QUESTIONS;
    }

    @GetMapping(path = "{questionId}")
    public Question getQuestion(@PathVariable("questionId") Integer questionId) {
        return QUESTIONS
          .stream()
          .filter(question -> questionId.equals(question.getQuestionId()))
          .findFirst()
          .orElseThrow(() ->
              new IllegalStateException("Question " + questionId + " does not exists"
           ));
    }
}
