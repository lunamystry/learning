package me.mandla.ally.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static me.mandla.ally.security.UserRole.*;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final PasswordEncoder passwordEncoder;

  @Autowired
  public SecurityConfiguration(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
  http
    .authorizeRequests()
    .antMatchers("/", "index", "/css/*", "/js/*")
      .permitAll()
    .antMatchers("/api/**")
      .hasAnyRole(USER.name(), SUDO.name())
    .anyRequest()
      .authenticated()
    .and()
    .httpBasic();
  }

  @Override
  @Bean
  protected UserDetailsService userDetailsService() {
    UserDetails ratiwe = User.builder()
                             .username("ratiwe")
                             .password(passwordEncoder.encode("password"))
                             .roles(USER.name())
                             .build();

    UserDetails sindi = User.builder()
                            .username("sindi")
                            .password(passwordEncoder.encode("password"))
                            .roles(SUDO.name())
                            .build();

    return new InMemoryUserDetailsManager(
        ratiwe,
        sindi
    );
  }
}
