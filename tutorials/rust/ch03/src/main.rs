use std::io;
mod phinda;

fn if_expression(num: i32) -> i32 {
    if num > 0 {
        num
    } else {
        0
    }
}


fn array_access() {
    let a = [1, 2, 3, 4, 5];

    println!("Please enter an array index.");

    let mut index = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line");

    let index: usize = index
        .trim()
        .parse()
        .expect("Index entered was not a number");

    let element = a[index];

    // a[5] = 6; a is not mutable. So I guess even compound types are immutable :-)

    println!(
        "The value of the element at index {} is: {}",
        index, element
    );
}

fn fizzbuzz(end: i32) {
    for n in 1..end+1 {
        if n % 15 == 0 {
            println!("fizzbuzz");
        } else if n % 3 == 0 {
            println!("fizz");
        } else if n % 5 == 0 {
            println!("buzz");
        } else {
            println!("{}", n);
        }
    }
}

fn print_u32(num: u32) {
    println!("{}", num);
}

fn main() {
    const name: &str = "Mhlobowami";
    // let name = "Usaphila";  You can't shadow a constant
    println!("Molo {}!", name);

    // Inference works :-)
    let guess = "42".parse().expect("Not a number!");
    print_u32(guess);

    fizzbuzz(20);

    array_access();

    println!("first number: {}", if_expression(-6));
    println!("second number: {}", if_expression(6));

    // phinda::njalonjalo(); // iphinda unaphakade!
    phinda::buthakathaka();
}
