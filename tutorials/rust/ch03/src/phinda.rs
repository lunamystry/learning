#[allow(dead_code)]
pub fn njalonjalo() {
    println!("Ngiyaqala!");
    loop {
        println!("Ngiyaphinda futhi!");
    }
}

/// Iyaphuka!
pub fn buthakathaka() {
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("iphuke ku: {}!", result);
}
