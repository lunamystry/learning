#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    // This is called an associated funciton (I am imagining a classmethod)
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

struct SignInLog {
    location: String,
    timestamp: String,
}

struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
    aliases: [String; 2],
    sign_ins: Vec<SignInLog>,
}

fn main() {
    let mut user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
        aliases: [String::from("umuntu"), String::from("somebody")],
        sign_ins: Vec::new(),
    };

    // You need to have the whole user mutable in order to change anything
    user1.email = String::from("anotheremail@example.com");
    user1.aliases[0] = String::from("omunye");
    user1.sign_ins.push(SignInLog { 
        timestamp: String::from("12H54"), 
        location: String::from("Delmas")
    });

    let rect = Rectangle {
        height: 50,
        width: 30,
    };

    let square = Rectangle::square(30);

    println!("rect is: {:?}", rect);
    println!("with area: {}", rect.area());
    println!("can rect hold square: {}", rect.can_hold(&square));
}

