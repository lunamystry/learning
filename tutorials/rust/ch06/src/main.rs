use std::fmt;
mod optional;

#[derive(Debug)]
enum IpAddrKind<'a> {
    V4(u8, u8, u8, u8),
    V6(&'a str),
}

// This shows that you can impl on enum similar to struct
//
// I wonder what the lifetimes here mean
impl <'a> fmt::Display for IpAddrKind<'a> {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        match self { 
            IpAddrKind::V4(a, b, c, d) => write!(f, "{}:{}:{}:{}", a, b, c, d),
            IpAddrKind::V6(value) => write!(f, "{}", value),
        }
    }
}


fn main() {
    let home = IpAddrKind::V4(127, 0, 0, 1);
    let loopback = IpAddrKind::V6("::1");

    display(&home);
    display(&loopback);

    print(&home);
    print(&loopback);

    optional::explore();
}

// Getting the value of an enum, you have to deal with all the enums
fn display(ip_addr: &IpAddrKind) {
    // Move the implementation to Display implementation above.
    println!("{}", ip_addr);
}

// A more complicated way of doing the match above. 
// Might be useful for just a single value.
// 
// from the docs:
//    However, the match expression can be a bit wordy in a situation in which we care about only
//    one of the cases. For this situation, Rust provides if let.
fn print(ip_addr: &IpAddrKind) {
    let r: String = if let IpAddrKind::V4(a, b, c, d) = ip_addr { 
        format!("{}:{}:{}:{}", a, b, c, d) 
    } else { 
        if let IpAddrKind::V6(value) = ip_addr { 
            value
        } else { 
            ""
        }.to_string()
    };

    println!("{}", r);
}
