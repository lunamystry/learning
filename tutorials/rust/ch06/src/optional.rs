pub fn explore() {
    println!("Exploring Option enum");
    variants()
}

fn variants() {
    println!("There are 2 variants: Some and None");
    // The type on the Some variant is optional: Option<u32>
    let x = Some(2);
    assert_eq!(x.is_some(), true);
    assert_eq!(x.is_none(), false);
    // The recommened way to get the value inside
    assert_eq!(x.unwrap(), 2);

    // The type on the None variant is required: Option<u32>
    let x: Option<u32> = None;
    assert_eq!(x.is_some(), false);
    assert_eq!(x.is_none(), true);
    // You can't unwrap what is not there. It is also a runtime error
    // assert_eq!(x.unwrap(), 2);
    // You can rather use unwrap_or
   assert_eq!(None.unwrap_or("bike"), "bike"); 

   // You can either unwrap or calculate from a closure
   let k = 10;
   let calculate = || 2 * k;
   assert_eq!(None.unwrap_or_else(calculate), 20);
   assert_eq!(None.unwrap_or_else(another_calculate), 20);

   // You can map
   let maybe_some_string = Some(String::from("Hello, World!"));
   // `Option::map` takes self *by value*, consuming `maybe_some_string`
   let maybe_some_len = maybe_some_string.map(|s| s.len());
   assert_eq!(maybe_some_len, Some(13));
}

fn another_calculate() -> u32 {
    // This would need to return a closure.
    2 * 10
}
