use std::process::Command;

fn main() {
    // You have to tell Rust this is a vector of i32s
    let explicit_vector: Vec<u32> = Vec::new();

    // But rust can infer the type too. (i32 is the default integer type)
    let inferred_vector = vec![1, 2, 3];


    // if it was clippy, it would say: "I see you gonna store i32s"
    let mut mutable_vector = Vec::new();

    mutable_vector.push(5);
    mutable_vector.push(6);
    mutable_vector.push(7);
    mutable_vector.push(8);

    // You can use iterators instead of for loops
    let a = vec![42, 0, 1];
    assert!(a.iter().any(|&x| x > 0));
    assert!(a.iter().all(|&x| x >= 0));

    // Building up to a flat_map
    let data = vec![vec![1, 2, 3, 4], vec![5, 6]];
    let flattened = data.into_iter().flatten().collect::<Vec<u8>>();
    assert_eq!(flattened, &[1, 2, 3, 4, 5, 6]);

    list_dir();
    println!("Hello, world!");
}

fn list_dir() {
    Command::new("ls")
            .args(&["-l", "-a"])
            // .arg("-l")
            // .arg("-a")
            .spawn()
            .expect("ls command failed to start");
}
