use structopt::StructOpt;
use anyhow::{Context, Result};
use std::{
    io::{self, BufReader, BufRead, BufWriter, Write},
    fs::File,
};
use nu_ansi_term::Color::Blue;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(StructOpt)]
struct Cli {
    /// The pattern to look for
    pattern: String,
    /// The path to the file to read
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::from_args();

    let f = File::open(&args.path)
        .with_context(|| format!("could not read file `{}`", args.path.display()))?;
    let reader = BufReader::new(f);

    let stdout = io::stdout(); // get the global stdout entity
    let mut handle = BufWriter::new(stdout); // optional: wrap that handle in a buffer

    for wrapped_line in reader.lines() {
        let line = wrapped_line.unwrap();
        if line.contains(&args.pattern) {
            writeln!(handle, "found: {}", Blue.paint(line))?;
        }
    }

    Ok(())
}
