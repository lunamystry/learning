fn main() {
    const name: &str = "Mhlobowami";
    // let name = "Usaphila";  You can't shadow a constant
    println!("Molo {}!", name);
}
