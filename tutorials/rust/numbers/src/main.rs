use std::marker::PhantomData;

struct Zero;
struct Succ<N: Nat>(PhantomData<N>);

trait Nat {}

impl Nat for Zero {}
impl<N: Nat> Nat for Succ<N> {}

struct Vector<N: Nat, A>(Vec<A>, PhantomData<N>);


fn main() {
    let _zero: Zero;
    let _one: Succ<Zero>;
    let _two: Succ<Succ<Zero>>;

    let v: Vector<Zero, u8> = Vector::<Zero, u8>::new();
    let _v_prime: Vector::<Succ<Zero>, u8> = v.cons(1);
}
