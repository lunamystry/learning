use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Kufanel' uqagel' i-number!");
    let secret_number = rand::thread_rng().gen_range(1..101);

    loop {
        println!("qagela: ");


        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Eish! alifundeki le line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,    
            Err(_) => {
                println!("Ak'siyi number le: {}", guess);
                continue;
            },
        };

        println!("uqagele: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("uqagele phansi!"),
            Ordering::Greater => println!("uqagele phezulu!"),
            Ordering::Equal => {
                println!("Uyitholile!");
                break;
            }
        }
    }
}
