# Thuphazana

A tool for creating thumbnails for things stored on wasabi via S3 API

## Features

```bash
  # Directories
  thuphazana thumbs s3:from/path/ s3:to/path/
  thuphazana thumbs file:from/path/ s3:to/path/
  thuphazana thumbs file:/absolute/from/path/ s3:to/path/
```

Future:

```bash
  thuphazana thumbs file:$HOME/template/from/path s3:to/path
  thuphazana thumbs s3:http://your-bucket.s3-website-us-east-1.wasabisys.com/fullurl/path s3:to/path
```
