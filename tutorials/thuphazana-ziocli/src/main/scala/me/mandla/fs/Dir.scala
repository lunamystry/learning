package me.mandla
package fs
package dir

import fs.Name.*
import fs.path.*

import scala.util.{ Try, Failure, Success }
import java.time.Instant

def mkdir(metadata: Metadata, children: Map[Name, FSTree]): Dir =
  // This returns a FSTree that is immediately turned into a Dir apparently.
  Dir(metadata, children)

def mkdir(name: Dirname, children: Map[Name, FSTree]): Dir =
  Dir(Metadata(dirname(name), 0, Instant.now), children)

def mkdir(name: Dirname): Dir = mkdir(name, Map())

def mkdir(dir: Dir, p: Path): Try[Dir] =
  val dirPath =
    p.filename match
      case None => p
      case Some(file) => Path(basepath(s"${p.basepath}/${file}"), None)

  val pathParts = dirPath.parts.filterNot(_.matches("^(/|\\./)$"))
  pathParts match
    case Nil => Success(dir)
    case name :: Nil =>
      findOrElse(dir, Path(name), mkdir(basepath(name)))
        .fold(
          (d: Dir) => Success(add(dir, d)),
          (f: File) => Failure(new IllegalStateException(s"cannot create $name is not a directory")),
        )
    case name :: remainingNames =>
      findOrElse(dir, Path(name), mkdir(basepath(name)))
        .fold(
          (d: Dir) => mkdir(d, Path(remainingNames)).map(add(dir, _)),
          (f: File) => Failure(new IllegalStateException(s"cannot create $name is not a directory")),
        )

def has(dir: Dir, file: File): Boolean = dir.children.contains(file.metadata.name)

def find(dir: Dir, searchPath: Path): Option[FSTree] =
  if searchPath.equals(Path("/")) || searchPath.equals(Path("./"))
  then Some(dir)
  else
    val pathParts = searchPath.parts.filterNot(_.matches("^(/|\\./)$"))
    pathParts match
      case Nil => None
      case child :: Nil =>
        dir.children.get(child)
      case parent :: children =>
        dir
          .children
          .get(parent)
          .fold(ifEmpty = None) { c =>
            c match
              case f: File =>
                throw IllegalStateException(s"parent ${parent} cannot be a file")
              case Dir(dm, dc) =>
                find(Dir(dm, dc), Path(children))
          }

def findOrElse(
  dir: Dir,
  searchPath: Path,
  default: FSTree,
): FSTree = find(dir, searchPath).getOrElse(default)

def add(dir: Dir, child: FSTree): Dir =
  child match
    case File(m, t) => Dir(dir.metadata, dir.children + (filename(m.name) -> File(m, t)))
    case Dir(m, c) => Dir(dir.metadata, dir.children + (dirname(m.name) -> Dir(m, c)))

def add(
  dir: Dir,
  path: Path,
  node: FSTree,
): Try[Dir] =
  val parentPath = Path(path.parts.init)
  if parentPath.isCurrentDir
  then Success(add(dir, node))
  else
    mkdir(dir, parentPath).flatMap { withParents =>
      find(withParents, parentPath)
        .fold(ifEmpty =
          Failure(
            new IllegalArgumentException(
              s"Could not add ${node}: ${parentPath} could not be created"
            )
          )
        ) { t =>
          t match
            case f: File =>
              Failure(
                new IllegalArgumentException(
                  s"Could not add ${node}: ${parentPath} is file not a directory"
                )
              )
            case parent: Dir =>
              val parentWithChild = add(parent, node)
              Success(add(dir, parentWithChild))
        }

    }

def map[B](dir: Dir, initial: B)(op: (B, FSTree) => B): B =
  val dirB: B = op(initial, dir)
  if dir.isEmpty
  then dirB
  else
    dir.children.values.foldLeft(dirB) { (acc, v) =>
      v match
        case d: Dir => map(d, acc)(op)
        case f: File => op(acc, f)
    }

// How I initially thought walk might work, but it is weird to use.
def walk[B](dir: Dir, initial: B)(op: (B, Dir) => B): B =
  val dirB: B = op(initial, dir)
  val dirs: List[Dir] =
    dir
      .children
      .values
      .filter(_.isInstanceOf[Dir])
      .map(_.asInstanceOf[Dir])
      .toList

  dirs.foldLeft(dirB) { (curr: B, childDir: Dir) =>
    walk(childDir, curr)(op)
  }

// Python like walk, easier to use.
def walk[B](dir: Dir, initial: B)(op: (B, Name, List[Dir], List[File]) => B): B =
  val name: Name = dir.metadata.name
  val dirs: List[Dir] =
    dir
      .children
      .values
      .filter(_.isInstanceOf[Dir])
      .map(_.asInstanceOf[Dir])
      .toList
  val files: List[File] =
    dir
      .children
      .values
      .filter(_.isInstanceOf[File])
      .map(_.asInstanceOf[File])
      .toList

  val dirB: B = op(initial, name, dirs, files)
  dirs.foldLeft(dirB) { (curr: B, childDir: Dir) =>
    walk(childDir, curr)(op)
  }

def filepaths(dir: Dir): Map[Path, File] =
  val init: (Path, Map[Path, File]) = (Path("/"), Map())
  val pathList: (Path, Map[Path, File]) =
    walk(dir, init) { (acc, dir) =>
      val name = dir.metadata.name
      val files: List[File] =
        dir
          .children
          .values
          .filter(_.isInstanceOf[File])
          .map(_.asInstanceOf[File])
          .toList
      val currPath = Path(s"${acc._1}/${name}")
      val filePaths: List[(Path, File)] =
        files.map { f =>
          (Path(s"$currPath/${f.metadata.name}"), f)
        }
      (currPath, acc._2 ++ filePaths)
    }

  pathList._2
