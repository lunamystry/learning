package me.mandla
package fs

export fs.FSTree.*

import java.time.Instant

type ImageFormat = String
type Dimensions = (Long, Long)

case class Metadata(
  name: Name,
  size: Size,
  lastModified: Instant,
)

enum FileType:
  case Text extends FileType
  case Image(format: ImageFormat, dimensions: Dimensions) extends FileType

enum FSTree:
  case Dir(metadata: Metadata, children: Map[Name, FSTree]) extends FSTree
  case File(metadata: Metadata, filetype: FileType) extends FSTree

  def fold[B](dirOp: Dir => B, fileOp: File => B): B =
    this match
      case dir: Dir => dirOp(dir)
      case file: File => fileOp(file)

  def isDir: Boolean =
    fold(_ => true, _ => false)
  def isFile: Boolean =
    fold(_ => false, _ => true)

  override def toString(): String =
    fold(
      dir => s"${dir.metadata.name}/${dir.children.values.mkString(",")}",
      file => s"${file.metadata.name}",
    )

// It is a good idea to put extensions in a object to prevent name collisions since
// it seems the signature is not used in identifying them.
object FSTree:
  extension (dir: FSTree.Dir) def isEmpty: Boolean = dir.children.isEmpty
