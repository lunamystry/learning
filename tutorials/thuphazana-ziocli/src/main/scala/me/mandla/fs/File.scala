package me.mandla
package fs

import fs.Name.*
import java.time.Instant

def touch(metadata: Metadata, filetype: FileType): File = File(metadata, filetype)

def touch(name: Filename): File = File(Metadata(filename(name), 0, Instant.now), FileType.Text)
