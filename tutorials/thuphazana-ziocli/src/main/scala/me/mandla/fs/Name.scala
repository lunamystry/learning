package me.mandla
package fs

import java.nio.charset.StandardCharsets

type Name = String
type Basepath = Name // ends with a /
type Dirname = Name // basepath with only one part
type Filename = Name // does not end with a /
type Size = Long
type ErrorMessage = String

object Name:
  extension (name: Name)
    def isEmpty: Boolean = name.isEmpty
    def length: Int = name.length

    def isValidBasepath: Option[ErrorMessage] =
      if name.isEmpty then Some("basepath cannot be empty")
      else if name.getBytes(StandardCharsets.UTF_8).length > 4096 then Some("basepath is too long")
      // The following should be allowable now
      else if name.equals(".") then Some("basepath cannot be '.'")
      else if name.equals("..") then Some("basepath cannot be '..'")
      else None
    def isBasepath: Boolean = isValidBasepath.isEmpty

    def isValidDirname: Option[ErrorMessage] =
      isValidBasepath match
        case Some(errors) => Some(errors)
        case None =>
          if name.trim.replaceAll("/+", "/").split("/").filter(_.nonEmpty).size > 1
          then Some("dirname cannot have multiple directories")
          else if name.equals("/") then Some("dirname cannot be '/'")
          else if name.equals("./") then Some("dirname cannot be './'")
          else None
    def isDirname: Boolean = isValidDirname.isEmpty

    def isValidFilename: Option[ErrorMessage] =
      if name.isEmpty then Some("filename cannot be empty")
      else if name.contains("/") then Some("filename cannot have '/'")
      else if name.trim.replaceAll("/+", "/").split("/").filter(_.nonEmpty).size > 1 then
        Some("filename cannot have directories")
      else if name.getBytes(StandardCharsets.UTF_8).length > 255 then Some("filename is too long")
      else if name.equals("./") then Some("dirname cannot be './'")
      else if name.equals(".") then Some("filename cannot be '.'")
      else if name.equals("..") then Some("filename cannot be '..'")
      else None
    def isFilename: Boolean = isValidFilename.isEmpty

  def dirname(name: String): Dirname =
    name.isValidDirname match
      case Some(error) =>
        throw new IllegalArgumentException(s"'${name}' is not a valid directory name ${error}")
      case None => basepath(name)

  def filename(name: String): Filename =
    name.isValidFilename match
      case Some(error) =>
        throw new IllegalArgumentException(s"'${name}' is not a valid file name ${error}")
      case None => name

  def basepath(name: String): Basepath =
    name.isValidBasepath match
      case Some(error) =>
        throw new IllegalArgumentException(s"'${name}' is not a valid basename: ${error}")
      case None =>
        (name
          .replaceAll("/+", "/")
          .replaceAll("/?$", "") + "/")
          .replaceAll("(\\./)+", "./")
