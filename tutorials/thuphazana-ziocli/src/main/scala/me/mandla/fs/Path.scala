package me.mandla
package fs
package path

import fs.Name.*

case class Path(basepath: Basepath, filename: Option[Filename]):
  require(basepath.isBasepath, "basepath must be valid.")
  require(filename.forall(_.isFilename), "filename must be valid if provided")

  def parts: List[String] =
    val baseparts =
      if basepath == "/"
      then List("/")
      else basepath.split("/").map(_ + "/").toList
    filename match
      case None => baseparts
      case Some(file) => baseparts :+ file

  def isBasepath: Boolean =
    filename.isEmpty

  def isRoot: Boolean =
    filename.isEmpty && basepath == "/"

  def isFilename: Boolean =
    filename.isDefined

  def isCurrentDir: Boolean =
    toString.equals("./")

  def name: Name =
    parts.last

  override def toString(): String =
    if isRoot
    then "/"
    else parts.mkString("/").replaceAll("/+", "/")

object Path:
  def apply(rawName: String): Path =
    if rawName.isBlank then throw new IllegalArgumentException("path cannot be empty")

    val parts =
      rawName
        .trim
        .replaceAll("/+", "/")
        .replaceAll("(\\./)+", "./")
        .split("/")
        .toList
        .filter(!_.isBlank)

    val root =
      if rawName.startsWith("/")
      then "/"
      else "./"
    val dir =
      if rawName == "/"
      then "/"
      else root + parts.init.mkString("/")
    val file =
      if rawName == "/"
      then ""
      else parts.last

    if rawName.endsWith("/")
    then
      if dir.isBlank
      then Path(basepath(root + file), None)
      else Path(basepath(s"${dir}/${file}"), None)
    else if dir.isBlank
    then Path(basepath(root), Some(filename(file)))
    else Path(basepath(dir), Some(filename(file)))

  def apply(parts: List[String]): Path =
    val a = parts.mkString("/").replaceAll("/+", "/")
    Path(a)
