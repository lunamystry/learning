package me.mandla
package s3

import scala.jdk.CollectionConverters.*
import scala.util.{ Try, Failure, Success }
import sttp.model.Uri.UriContext

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.{ GetObjectRequest, ListObjectsV2Request, S3Object }

import org.apache.tika.Tika

import fs.*
import fs.Name.*
import fs.path.*
import fs.dir.*

val WASABI_URL = uri"https://s3.eu-central-2.wasabisys.com/"

def client(): S3Client =
  val accessKey =
    sys.env.getOrElse("WASABI_ACCESS_KEY", throw new Exception("WASABI_ACCESS_KEY is not set"))
  val secretKey =
    sys.env.getOrElse("WASABI_SECRET_KEY", throw new Exception("WASABI_SECRET_KEY is not set"))
  val credentials = AwsBasicCredentials.create(accessKey, secretKey)

  val client =
    S3Client
      .builder()
      .region(Region.US_EAST_1) // Amazon does not have an eu-central-2
      .credentialsProvider(StaticCredentialsProvider.create(credentials))
      .endpointOverride(WASABI_URL.toJavaUri)
      .build();
  client

def addDir(dir: Dir, o: S3Object): Try[Dir] =
  val path = Path(o.key)
  val metadata = Metadata(dirname(path.name), o.size, o.lastModified)
  add(dir, path, Dir(metadata, Map()))

def addFile(dir: Dir, o: S3Object): Try[Dir] =
  val path = Path(o.key)
  val metadata = Metadata(filename(path.name), o.size, o.lastModified)
  val filetype = getFileType(o)
  add(dir, path, touch(metadata, filetype))

// TODO: embed the file type information in the key of the file
//   e.g picture-100x203.jpg
def getFileType(o: S3Object): FileType =
  if o.key.endsWith(".jpg")
  then FileType.Image("jpeg", (1, 2))
  else FileType.Text

// Too expensive to run on every object.
def sniffFileType(o: S3Object): FileType =
  val getObjectRequest =
    GetObjectRequest
      .builder()
      .bucket("lycloud")
      .key(o.key)
      .build();
  val responseInputStream = client().getObject(getObjectRequest);
  val tika = new Tika()
  val fileType = tika.detect(responseInputStream)
  print(fileType)
  FileType.Text

def list(bucketName: Name): Try[Dir] =
  val s3Client = client()
  Try:
    val request = ListObjectsV2Request.builder().bucket(bucketName).build()
    val content = s3Client.listObjectsV2(request).contents().asScala.toList

    content.foldLeft(mkdir(bucketName)) { (r: Dir, o: S3Object) =>
      if o.key.endsWith("/")
      then
        addDir(r, o) match
          case Failure(error) =>
            println(s"An error occured while adding dir: ${error}")
            r
          case Success(d) => d
      else
        addFile(r, o) match
          case Failure(error) =>
            println(s"An error occured while adding file: ${error}")
            r
          case Success(d) => d
    }
