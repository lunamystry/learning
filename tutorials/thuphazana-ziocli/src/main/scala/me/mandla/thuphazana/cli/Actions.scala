package me.mandla
package thuphazana
package cli

import zio.cli.*
import zio.cli.HelpDoc.Span.text
import zio.Console.printLine

def thuphazanaCliApp =
  CliApp.make(
    name = "Thuphazana",
    version = "0.0.1",
    summary = text("Upload thumbnails and remove exif stuff to s3"),
    command = git,
  ):
    case Subcommand.Add(modified, directory) =>
      printLine(s"Executing `git add $directory` with modified flag set to $modified")
    case Subcommand.Remote.Add(name, url) =>
      printLine(s"Executing `git remote add $name $url`")
    case Subcommand.Remote.Remove(name) =>
      printLine(s"Executing `git remote remove $name`")
    case Subcommand.Remote(verbose) =>
      printLine(s"Executing `git remote` with verbose flag set to $verbose")
