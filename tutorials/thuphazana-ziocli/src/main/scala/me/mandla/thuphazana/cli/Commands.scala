package me.mandla
package thuphazana
package cli

import zio.cli.*

import fs.Name.*
import fs.path.*
import cli.Izenzo.Izithupha

type FromPath = Path
type ToPath = Path

enum Izenzo extends Serializable:
  case Izithupha(
    from: FromPath,
    to: ToPath,
  ) extends Izenzo

val thuphazanaHelp: HelpDoc = HelpDoc.p("Thuphazana help")
val thuphazana: Command[Izenzo] =
  Command("thumbs", Options.none, Args.none)
    .withHelp(thuphazanaHelp)
    .map { f =>
      Izithupha(Path("from/"), Path("to/"))
    }

object FromPathArgs extends Args[FromPath]:
  def read(string: String): Either[String, FromPath] =
    try Right(FromPath(string.toInt))
    catch
      case _: NumberFormatException =>
        Left(s"Invalid value for FromPath: $string")

  def write(value: FromType): String =
    value.value.toString

// object ToPathArgs extends Args[ToPath]:
//   def read(string: String): Either[String, ToPath] =
//     try Right(ToPath(string.toInt))
//     catch
//       case _: NumberFormatException =>
//         Left(s"Invalid value for ToPath: $string")
//
//   def write(value: ToType): String =
//     value.value.toString

// sealed trait Subcommand extends Product with Serializable
// object Subcommand:
//   final case class Add(modified: Boolean, directory: JPath) extends Subcommand
//   final case class Remote(verbose: Boolean) extends Subcommand
//   object Remote:
//     sealed trait RemoteSubcommand extends Subcommand
//     final case class Add(name: String, url: String) extends RemoteSubcommand
//     final case class Remove(name: String) extends RemoteSubcommand
//
// val modifiedFlag: Options[Boolean] = Options.boolean("m")
// val addHelp: HelpDoc = HelpDoc.p("Add subcommand description")
// val add =
//   Command("add", modifiedFlag, Args.directory("directory"))
//     .withHelp(addHelp)
//     .map:
//       case (modified, directory) =>
//         Subcommand.Add(modified, directory)
//
// val verboseFlag: Options[Boolean] = Options.boolean("verbose").alias("v")
// val configPath: Options[JPath] = Options.directory("c", Exists.Yes)
// val remoteAdd =
//   val remoteAddHelp: HelpDoc = HelpDoc.p("Add remote subcommand description")
//   Command("add", Options.text("name") ++ Options.text("url"))
//     .withHelp(remoteAddHelp)
//     .map:
//       case (name, url) =>
//         Subcommand.Remote.Add(name, url)
//
// val remoteRemove =
//   val remoteRemoveHelp: HelpDoc = HelpDoc.p("Remove remote subcommand description")
//   Command("remove", Args.text("name"))
//     .withHelp(remoteRemoveHelp)
//     .map(Subcommand.Remote.Remove.apply)
//
// val remoteHelp: HelpDoc = HelpDoc.p("Remote subcommand description")
// val remote =
//   Command("remote", verboseFlag)
//     .withHelp(remoteHelp)
//     .map(Subcommand.Remote(_))
//     .subcommands(remoteAdd, remoteRemove)
//     .map(_._2)
//
// val git: Command[Subcommand] = Command("git", Options.none, Args.none).subcommands(add, remote)
