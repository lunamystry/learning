package me.mandla
package fs
package dir

import fs.Name.*
import fs.path.*

final class DirSuite extends TestSuite:
  test("mkdir should be able to create and empty directory"):
    val dir = mkdir("/empty")
    assert(dir.metadata.name == "/empty/")
    assert(dir.children.size == 0)

  test("mkdir be able to copy directory with metadata"):
    val dir = mkdir("/dir")
    assert(dir.metadata.name == "/dir/")
    assert(dir.children == Map())

    val dir2 = mkdir(dir.metadata, dir.children)
    assert(dir2.metadata.name == "/dir/")
    assert(dir2.children == Map())

  test("dir can be added to"):
    val dir = mkdir("/dir")
    assert(dir.children == Map())

    val withAddedChild = add(dir, dir) // this should be blocked
    assert(withAddedChild.children.get(dir.metadata.name).isDefined)

  test("dir can be added to with metadata"):
    val dir = mkdir("/dir")
    val file = touch("filename")
    assert(dir.children == Map())

    val withAddedChild = add(dir, file)
    assert(withAddedChild.children.get(file.metadata.name).isDefined)

  test("dir can be used with fold"):
    val dir = mkdir("/dir")
    assert(dir.metadata.name == "/dir/")
    assert(dir.children == Map())

    Dir(dir.metadata, dir.children).fold(
      dir =>
        assert(dir.metadata.name == "/dir/")
        assert(dir.children == Map())
      ,
      _ => fail("Should not happen"),
    )

  test("has should be able to find a file in direct children"):
    val file = touch("file")
    val dir = mkdir("name", Map(filename("file") -> file))
    assert(has(dir, file))

  test("find should be able to get immediate child in directory"):
    val file = touch("file")
    val dir = mkdir("name", Map(filename("file") -> file))

    find(dir, Path("file"))
      .fold(() => fail(s"expected file not found")) { (node: FSTree) =>
        node match
          case File(m, t) => assert(m.name == file.metadata.name)
          case p: Dir => fail(s"expected file found a directory instead")
      }

  test("find should be able to get path with parent and child"):
    val file = touch("file")
    val parent = mkdir("parent", Map(filename("file") -> file))
    val dir = mkdir("name", Map(basepath("parent") -> parent))

    find(dir, Path("/parent/file"))
      .fold(() => fail(s"expected file not found")) { (node: FSTree) =>
        node match
          case File(m, t) => assert(m.name == file.metadata.name)
          case p: Dir => fail(s"expected file found a directory instead")
      }

  test("find should be able to get current dir"):
    val dir = mkdir("name", Map())

    find(dir, Path("./"))
      .fold(() => fail(s"expected file not found")) { (node: FSTree) =>
        node match
          case Dir(m, c) => assert(m.name == dir.metadata.name)
          case f: File => fail(s"expected directory found a file instead")
      }

  test("find should be able to get path to file in directory"):
    val file = touch("file")
    val parent = mkdir("parent", Map(filename("file") -> file))
    val grandparent = mkdir("grandparent", Map(basepath("parent") -> parent))
    val dir = mkdir("name", Map(grandparent.metadata.name -> grandparent))

    find(dir, Path("grandparent/parent/file"))
      .fold(() => fail(s"expected file not found")) { (node: FSTree) =>
        node match
          case File(m, t) => assert(m.name == file.metadata.name)
          case p: Dir => fail(s"expected file found a directory instead")
      }

  test("find should be able to get path to directory in directory"):
    val child = mkdir("child")
    val parent = mkdir("parent", Map(child.metadata.name -> child))
    val grandparent = mkdir("grandparent", Map(parent.metadata.name -> parent))
    val dir = mkdir("name", Map(grandparent.metadata.name -> grandparent))

    find(dir, Path("grandparent/parent/child/"))
      .fold(() => fail(s"expected file not found")) { (node: FSTree) =>
        node match
          case Dir(m, c) => assert(m.name == child.metadata.name)
          case f: File => fail(s"expected directory found a file instead")
      }

  test("mkdir should create parent of one child"):
    val root = mkdir("root")
    mkdir(root, Path("lvl1")).fold(
      error => fail(s"${error} mkdirP failed"),
      withLvl1 => assert(find(withLvl1, Path("lvl1/")).isDefined),
    )

  test("mkdir should create parent of 2 levels"):
    mkdir(mkdir("root"), Path("lvl1"))
      .fold(
        error => fail(s"${error} mkdir failed"),
        withLvl1 =>
          assert(find(withLvl1, Path("lvl1/")).isDefined)
          mkdir(withLvl1, Path("l1/l2")),
      )
      .fold(
        error => fail(s"${error} mkdir failed"),
        r =>
          assert(r.children.size == 2)
          assert(find(r, Path("lvl1/")).isDefined)
          assert(find(r, Path("l1/")).isDefined),
      )

  test("mkdir should create parent of 2 levels even if one exists"):
    mkdir(mkdir("root"), Path("lvl1"))
      .fold(
        error => fail(s"${error} mkdirP failed"),
        withLvl1 =>
          assert(find(withLvl1, Path("lvl1/")).isDefined)
          mkdir(withLvl1, Path("lvl1/l1/l2")),
      )
      .fold(
        error => fail(s"${error} mkdirP failed"),
        r =>
          assert(r.children.size == 1)
          assert(find(r, Path("lvl1/l1/")).isDefined)
          assert(find(r, Path("lvl1/l1/l2/")).isDefined),
      )

  test("map should be able to map dir"):
    val file = touch("filename")
    val file2 = touch("file2")
    val file3 = touch("file3")
    val dir = add(add(mkdir("/dir"), file), file3)
    val root = add(add(mkdir("/root"), dir), file2)

    val list: List[FSTree] =
      map(root, List()) { (ls, file) =>
        ls ++ List(file)
      }

    assert(list.size == 5)

  test("walk should usable to create list of paths"):
    val dir =
      add(
        add(mkdir("/dir"), touch("filename")),
        touch("file2"),
      )
    val root = add(add(mkdir("/root"), add(mkdir("/dir2"), dir)), touch("file3"))

    val init: (Path, List[Path]) = (Path("./"), List())
    val fileList: (Path, List[Path]) =
      walk(root, init):
        (
          acc,
          name,
          dirs,
          files,
        ) =>
          val currPath = Path(s"${acc._1}/${name}")
          val filePaths =
            files.map { f =>
              Path(s"$currPath/${f.metadata.name}")
            }
          (currPath, acc._2 ++ filePaths)

    assert(fileList._2.size == 3)

  test("walk should usable to create list of paths (with dir)"):
    val dir =
      add(
        add(mkdir("/dir"), touch("filename")),
        touch("file2"),
      )
    val root = add(add(add(mkdir("/root"), mkdir("/dir2")), dir), touch("file3"))

    val init: (Path, List[Path]) = (Path("./"), List())
    val fileList: (Path, List[Path]) =
      walk(root, init) { (acc, dir) =>
        val name = dir.metadata.name
        val files: List[File] =
          dir
            .children
            .values
            .filter(_.isInstanceOf[File])
            .map(_.asInstanceOf[File])
            .toList
        val currPath = Path(s"${acc._1}/${name}")
        val filePaths =
          files.map { f =>
            Path(s"$currPath/${f.metadata.name}")
          }
        (currPath, acc._2 ++ filePaths)
      }

    assert(fileList._2.size == 3)

  test("filepaths should able to get paths from dir"):
    val dir =
      add(
        add(mkdir("/dir"), touch("filename")),
        touch("file2"),
      )
    val root = add(add(add(mkdir("/root"), mkdir("/dir2")), dir), touch("file3"))

    val pathList: Map[Path, FSTree] = filepaths(root)
    assert(pathList.size == 3)
