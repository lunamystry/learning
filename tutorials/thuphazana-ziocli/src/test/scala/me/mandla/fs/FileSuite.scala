package me.mandla
package fs

final class FileSuite extends TestSuite:
  test("should be able to create and empty file"):
    val file = touch("name")
    assert(file.metadata.name == "name")

  test("be able to copy file with metadata"):
    val file = touch("name")
    assert(file.metadata.name == "name")
    val file2 = touch(file.metadata, file.filetype)
    assert(file2.metadata.name == "name")
