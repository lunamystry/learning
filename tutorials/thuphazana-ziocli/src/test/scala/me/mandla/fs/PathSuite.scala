package me.mandla
package fs

import fs.Name.*

import fs.path.*
import org.scalacheck.Gen
import org.scalacheck.Prop.forAll

final class PathSuite extends TestSuite:
  test("can create basepaths"):
    val basepaths: Gen[String] = Gen.alphaNumStr.map(_ + "/")

    val prop =
      forAll(basepaths) { name =>
        assert(Path(name).isBasepath)
      }

    prop.check()

  test("can create a / as basepath (root)"):
    assert(Path("/").isBasepath)

  test("can convert correctly to string"):
    assert(Path("/").toString == "/")
    assert(Path("/root").toString == "/root")
    assert(Path("/root/").toString == "/root/")
    assert(Path("/root/dir/").toString == "/root/dir/")
    assert(Path("/root/file").toString == "/root/file")

  test("can handle current dir"):
    assert(Path("././././././").basepath == "./")

  test("can create a valid filename"):
    assert(Path("name.ext").isFilename)
    assert(Path(".hidden").isFilename)
    assert(Path("..hiddendot").isFilename)
    assert(Path("name").isFilename)
    assert(Path("/root/file/name.ext").isFilename)
    assert(Path("relative/file/name.ext").isFilename)

  test("can be created from parts for a basepath"):
    val testPath: Path = Path("a/path/to/here/")
    val parts = testPath.parts
    assert(parts == List("./", "a/", "path/", "to/", "here/"))
    val recreated: Path = Path(parts)
    assert(testPath == recreated)

  test("can be created from parts for a filename"):
    val testPath: Path = Path("a/path/to/here")
    val parts = testPath.parts
    val recreated: Path = Path(parts)
    assert(testPath == recreated)

  test("retain the slashes for directories in a filename"):
    val testPath: Path = Path("a/path/to/here")
    val parts = testPath.parts

    assert(parts.init.forall(_.endsWith("/")))
    assert(!parts.last.endsWith("/"))

  test("retain the slashes for directories in a basepath"):
    val testPath: Path = Path("a/path/to/here/")
    val parts = testPath.parts
    assert(parts.forall(_.endsWith("/")))

  test("retain the root slash (dir)"):
    val testPath: Path = Path("/a/path/to/here/")
    val parts = testPath.parts
    assert(parts.head == "/")
    assert(parts.forall(_.endsWith("/")))

  test("retain the root slash (file)"):
    val testPath: Path = Path("/a/path/to/here")
    val parts = testPath.parts
    assert(parts.head == "/")

  for invalidName <-
        List(
          "",
          ".",
          "..",
        )
  do
    test(s"throw IllegalArgumentException for basepath(\"$invalidName\")"):
      intercept[java.lang.IllegalArgumentException]:
        basepath(invalidName)

  for name <-
        List(
          // without slashes
          "name.ext",
          ".hidden",
          "..hiddendot",
          "name",
          "./", // path
          "/root/file/name.ext", // path
          "relative/file/name.ext", // path
          // with slashes
          "/",
          "name.ext/",
          ".hidden/",
          "..hiddendot/",
          "name/",
          "/root/file/name.ext/", // path
          "relative/file/name.ext/", // path
        )
  do
    test(s"able to create a basepath from $name"):
      assert(basepath(name).isBasepath)

  for param <-
        List(
          "///" -> "/",
          "//this///is/a/" -> "/this/is/a/",
          "///name//" -> "/name/",
          "noslash" -> "noslash/",
        )
  do
    test(s"clean up '${param._1}' -> '${param._2}"):
      val base = basepath(param._1)
      assert(base.isBasepath)
      assert(base == param._2)

  for invalidName <-
        List(
          "",
          ".",
          "..",
          "/",
          "./", // path
          "/root/file/name.ext", // path
          "relative/file/name.ext", // path
          // with slashes
          "/root/file/name.ext/", // path
          "relative/file/name.ext/", // path
        )
  do
    test(s"throws IllegalArgumentException for dirname(\"${invalidName}\")"):
      intercept[java.lang.IllegalArgumentException]:
        dirname(invalidName)

  for name <-
        List(
          // without slashes
          "name.ext",
          ".hidden",
          "..hiddendot",
          "name",
          // with slashes
          "name.ext/",
          ".hidden/",
          "..hiddendot/",
        )
  do
    test(s"be able to create a valid dirname from '$name'"):
      assert(dirname(name).isDirname)

  for invalidName <-
        List(
          "",
          ".",
          "..",
          "/",
          "name/",
          "/root/file/name.ext", // path
          "relative/file/name.ext", // path
        )
  do
    test(s"throw IllegalArgumentException for filename(\"$invalidName\")"):
      intercept[java.lang.IllegalArgumentException]:
        filename(invalidName)

  for name <-
        List(
          "filename",
          "name.ext",
          ".hidden",
          "..hiddendot",
          "name",
        )
  do
    test(s"be able to create a valid filename from '$name'"):
      assert(filename(name).isFilename)
