package me.mandla
package s3

import fs.*

import scala.util.Try

final class S3Suite extends TestSuite:
  test("read"):
    val lycloud: Try[Dir] = list("lycloud")
    lycloud.fold(
      error => fail(s"Failed, message is: $error"),
      root => println(root),
    )
