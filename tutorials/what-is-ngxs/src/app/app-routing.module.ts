import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from '@/about/about.component';
import { WelcomeComponent } from '@/welcome/welcome.component';
import { LoginComponent } from '@/login/login.component';
import { HomeComponent } from '@/home/home.component';
import { PageNotFoundComponent } from '@/page-not-found/page-not-found.component';
import { LoginGuard } from '@/login/login.models';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'welcome' },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [LoginGuard] },
  { path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
