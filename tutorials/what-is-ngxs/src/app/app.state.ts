import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';

import { Login, Logout } from '@/login/login.actions';
import { LoginService } from '@/login/login.service';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { User } from '@/login/login.models';
import { GetResource } from '@/welcome/welcome.actions';
import { HttpClient } from '@angular/common/http';

export interface AppStateModel {
  user: User;
  greeting: { id: string, content: string };
  questions: string[];
}

const defaultUser = { email: null, token: null };

@State<AppStateModel>({
  name: 'app',
  defaults: {
    user: defaultUser,
    greeting: { id: 'XXX', content: 'Hello World' },
    questions: []
  }
})
@Injectable()
export class AppState {

  constructor(public loginService: LoginService, private http: HttpClient) { }

  @Selector()
  static token(state: AppStateModel): string | null {
    return state.user.token;
  }

  @Selector()
  static greeting(state: AppStateModel): { id: string, content: string } | null {
    return state.greeting;
  }

  @Selector()
  static isAuthenticated(state: AppStateModel): boolean {
    return !!state.user.token;
  }

  @Action(Login)
  login({ patchState }: StateContext<AppStateModel>, action: Login): Observable<User> {
    return this.loginService
      .login(action.email, action.password)
      .pipe(tap(user => {
        patchState({ user });
      }));
  }

  @Action(Logout)
  logout({ patchState }: StateContext<AppStateModel>): Observable<null> {
    return this.loginService
      .logout()
      .pipe(tap(_ => {
        patchState({ user: defaultUser });
      }));
  }

  @Action(GetResource)
  getResource({ patchState }: StateContext<AppStateModel>): any {
    return this.http.get<{ id: string, content: string }>('http://localhost:4200/api/resource').subscribe(greeting => {
      patchState({ greeting });
    });
  }
}
