export class Login {
    static readonly type = '[Login] login';
    constructor(public email: string, public password: string) {}
}

export class Logout {
    static readonly type = '[Login] logout';
}
