import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { AppState } from '@/app/app.state';

import { Login, Logout } from './login.actions';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isAuthenticated$: Observable<boolean>;
  loginForm!: FormGroup;

  constructor(private store: Store, private fb: FormBuilder) {
    this.isAuthenticated$ = this.store.select(AppState.isAuthenticated);
  }

  login(): void {
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;

    this.store.dispatch(new Login(email, password));
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.loginForm = this.fb.group({
      email: '',
      password: '',
    });
  }
}
