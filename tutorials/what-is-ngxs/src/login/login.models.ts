import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';

import { AppState } from '@/app/app.state';

export interface User {
  email: string | null;
  token: string | null;
}

@Injectable({ providedIn: 'root' })
export class LoginGuard implements CanActivate {
  constructor(private store: Store) {}

  canActivate(): boolean {
    const isAuthenticated = this.store.selectSnapshot(AppState.isAuthenticated);
    return isAuthenticated;
  }
}
