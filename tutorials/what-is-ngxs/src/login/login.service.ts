import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';

import { User } from './login.models';


@Injectable({ providedIn: 'root' })
export class LoginService {
  authUrl = 'https://localhost:4200/api';

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<User> {
    const headers = new HttpHeaders({
      authorization: 'Basic ' + btoa(email + ':' + password)
    });

    return this.http
      .get<any>(`${this.authUrl}/user`, { headers })
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError), // then handle the error
        map(json => ({ email: json.body, token: json.title })),
      );
  }

  logout(): Observable<null> {
    return this.http.post<any>(`${this.authUrl}/logout`, {})
      .pipe(
        retry(3), // really, should one retry logout?
        catchError(this.handleError), // then handle the error
      );
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
