import { AppState } from '@/app/app.state';
import { Logout } from '@/login/login.actions';
import { User } from '@/login/login.models';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  user$: Observable<User>;
  isAuthenticated$: Observable<boolean>;

  constructor(private store: Store) {
    this.user$ = this.store.select(state => state.app.user);
    this.isAuthenticated$ = this.store.select(AppState.isAuthenticated);
  }

  ngOnInit(): void {
  }

  logout(): void {
    this.store.dispatch(new Logout());
  }
}
