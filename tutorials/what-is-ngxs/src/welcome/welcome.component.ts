import { AppState, AppStateModel } from '@/app/app.state';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GetResource } from './welcome.actions';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  @Select(AppState.greeting) greeting$ !: Observable<{ id: string, content: string }>;

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(GetResource);
  }

}
