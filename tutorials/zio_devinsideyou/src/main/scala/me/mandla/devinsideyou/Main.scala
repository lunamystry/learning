package me.mandla
package devinsideyou

import ourzio.*

object Main extends scala.App:
  lazy val program =
    for
      _ <- console.putStrLn("─" * 80)

      _ <- console.putStrLn("Uban' igama lakho: ")

      // name <- console.getStrLn
      name <- ZIO.succeed("Shozoku")
      _ <- console.putStrLn(s"Sawubona $name!")

      // _ <- ZIO.fail("Booom!")

      _ <- console.putStrLn("─" * 80)
    yield ()

  Runtime.default.unsafeRunSync(program)
