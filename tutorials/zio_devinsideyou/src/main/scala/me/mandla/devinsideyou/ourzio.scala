package me.mandla
package devinsideyou

import me.mandla.devinsideyou.ourzio.ZIO.succeed

object ourzio:
  // ## ZIO
  //
  // ZIO is bascially a type that has a thunk
  //
  // A thunk is a function that has not input but can do things (a delayed calculation) - https://en.wikipedia.org/wiki/Thunk
  //
  // The way it is used here, I wanna call it deferred rather than delayed because delayed is kinda automatic in my mind.
  // Like the calculation will happen on it's on. Deferred reminds me of the need to collapse the ZIO i.e Runtime.default.unsafeRunSync
  //
  // ## by-name parameter
  //
  // a parameter with `=>` prepended to its type. Evaluated every time it is used, not evaluated if not used.
  //
  // ## parametric polymorphism vs subtype polymorphism
  //
  // ## declaration side parametric polymorphism
  //
  // You have control over the parameters when you declare them (scala) not when you call them (java)
  //
  // ## subtype vs inheritance
  //
  // ## Variance
  //
  // convariant A occurs in contravariant position: An output type is being used on an input position of a function
  //
  // ### covariant
  //  - you'll see them with the `+` e.g `+A`
  //  - Java has this one built in.
  //  - Types being returned
  //
  // ### contravariant
  //  - you'll see them with the `-` e.g `-E`
  //  - Types being passed in
  //
  final case class ZIO[+E, +A](thunk: () => Either[E, A]):
    // `E1 >: E` means an E1 that is atleast an E
    def flatMap[E1 >: E, B](azb: A => ZIO[E1, B]): ZIO[E1, B] =
      ZIO { () =>
        val errorOrA = thunk()

        val zErrorOrB = errorOrA match
          case Right(a) => azb(a)
          case Left(e) => ZIO.fail(e)

        val b = zErrorOrB.thunk()
        b
      }

    // When you have a bi-functor, it is usually Right biased
    def map[B](ab: A => B): ZIO[E, B] =
      ZIO { () =>
        val errorOrA = thunk()

        var zErrorOrB = errorOrA match
          case Right(a) => Right(ab(a))
          case Left(e) => Left(e)

        zErrorOrB
      }

    // This function is basically a flatMap for errors
    // `A1 >: A` means an A1 that is atleast A
    def catchAll[E2, A1 >: A](h: E => ZIO[E2, A1]): ZIO[E2, A1] =
      ZIO { () =>
        val errorOrA = thunk()

        val zErrorOrB = errorOrA match
          case Right(a) => ZIO.succeed(a)
          case Left(e) => h(e)

        val b = zErrorOrB.thunk()
        b
      }

    def mapError[E2](h: E => E2): ZIO[E2, A] =
      ZIO { () =>
        val errorOrA = thunk()
        val errorOrA1 = errorOrA match
          case Right(a) => Right(a)
          case Left(e) => Left(h(e))

        errorOrA1
      }

  object ZIO:
    def succeed[A](a: => A): ZIO[Nothing, A] =
      ZIO(() => Right(a))

    def fail[E](e: => E): ZIO[E, Nothing] =
      ZIO(() => Left(e))

    def effect[A](a: => A): ZIO[Throwable, A] =
      ZIO { () =>
        try Right(a)
        catch Left(_)
      }

  object console:
    def putStrLn(line: String) =
      ZIO.succeed(println(line))

    val getStrLn =
      ZIO.succeed(scala.io.StdIn.readLine())

  object Runtime:
    object default:
      def unsafeRunSync[E, A](zio: => ZIO[E, A]): Either[E, A] =
        zio.thunk()
