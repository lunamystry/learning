package me.mandla
package zionomicon

import zio.*
import zio.Console.printLine
import scala.io.StdIn
import java.time.temporal.ChronoUnit

object FiggusApp extends ZIOAppDefault:
  val readLine =
    ZIO.attempt(StdIn.readLine())

  def printLn(line: String) =
    ZIO.attempt(println(line))

  def readAndPrint =
    readLine.flatMap(line => printLn(line))

  val printNumbers =
    ZIO.foreach(1 to 100) { n =>
      printLine(n.toString)
    }

  val prints =
    List(
      printLine("The"),
      printLine("quick"),
      printLine("brown"),
      printLine("fox"),
    )

  val printWords =
    ZIO.collectAll(prints)

  final case class MyZIO[-R, +E, +A](run: R => Either[E, A]):
    self =>
    def map[B](f: A => B): MyZIO[R, E, B] =
      MyZIO(r => self.run(r).map(f))

  override def run =
    printWords.delay(Duration(5, ChronoUnit.SECONDS))
